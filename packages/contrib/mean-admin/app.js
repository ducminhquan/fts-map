'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;
var Admin = new Module('mean-admin');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */

Admin.register(function (app, auth, database) {
    Admin.aggregateAsset('css', 'admin.css');
    Admin.aggregateAsset('css', 'themes.css');
    Admin.aggregateAsset('js', 'users.js');
    Admin.aggregateAsset('js', 'themes.js');
    Admin.aggregateAsset('js', 'modules.js');
    Admin.aggregateAsset('js', 'ng-clip.js');
    Admin.aggregateAsset('js', 'settings.js');
    Admin.aggregateAsset('js', '../lib/ng-clip/src/ngClip.js', {
        absolute: false,
        global: true
    });

    var icons = 'mean-admin/assets/img/icons/'

    Admin.menus.add({
        'roles': ['admin'],
        'title': 'Admin',
        'dropdown': [{
            'title': 'Modules',
            'link': 'modules',
            'icon': icons + 'modules.png'
        }, {
            'roles': ['admin'],
            'title': 'Themes',
            'link': 'themes',
            'icon': icons + 'themes.png'
        }, {
            'roles': ['admin'],
            'title': 'Settings',
            'link': 'settings',
            'icon': icons + 'settings.png'
        }, {
            'roles': ['admin'],
            'title': 'Users',
            'link': 'users',
            'icon': icons + 'users.png'
        }]
    });

    Admin.aggregateAsset('js', '../lib/zeroclipboard/dist/ZeroClipboard.min.js', {
        absolute: false,
        global: true
    });

    Admin.angularDependencies(['ngClipboard']);

    // We enable routing. By default the Package Object is passed to the routes
    Admin.routes(app, auth, database);
    return Admin;
});
