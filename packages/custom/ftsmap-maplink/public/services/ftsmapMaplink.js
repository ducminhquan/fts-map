'use strict';

//Articles service used for articles REST endpoint
angular.module('mean.ftsmap-maplink').factory('FtsmapMaplink', ['$resource',
  function ($resource) {
      return $resource('maplink/:maplinkId', {
          maplinkId: '@_id'
      }, {
          getMapLink: {
              url: '/maplink/get',
              method: 'GET'
          }
      });
  }
]);
