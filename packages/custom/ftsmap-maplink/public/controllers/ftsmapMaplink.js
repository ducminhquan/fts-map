'use strict';

angular.module('mean.ftsmap-maplink').controller('MaplinkController', ['$scope', 'Global', 'FtsmapMaplink',
  function ($scope, Global, FtsmapMaplink) {
      $scope.global = Global;



      $scope.center = {
          lat: 10.7863767,
          lng: 106.6758813,
          zoom: 13
      };

      $scope.markers = [];


      $scope.layers = {
          baselayers: {
              ftsmap: {
                  name: 'FTS Map',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              }
          }
      };



      $scope.playgroundInit = function () {
          var drawGeoJson = function (data) {
              $scope.geojson = {
                  data: data,
                  pointToLayer: function (feature, latlng) {
                      return L.marker(latlng, { icon: L.AwesomeMarkers.icon({ markerColor: feature.properties.color, icon: feature.properties.icon }) });
                  }
              };
          };

          $scope.zoom = 17;
          $scope.detail = false;

          $scope.$on('leafletDirectiveMap.click', function (event, arg) {

              var latlng = arg.leafletEvent.latlng;

              $scope.center = {
                  lat: latlng.lat,
                  lng: latlng.lng,
                  zoom: 17
              };

              if ($scope.markers.length === 0) {
                  $scope.markers.push({
                      lat: latlng.lat,
                      lng: latlng.lng,
                      focus: true,
                      draggable: false
                  });
              } else {
                  $scope.markers[0].lat = latlng.lat;
                  $scope.markers[0].lng = latlng.lng;
              }



              var params = {
                  lat: $scope.markers[0].lat,
                  lng: $scope.markers[0].lng,
                  zoom: $scope.zoom,
                  detail: $scope.detail
              };

              FtsmapMaplink.getMapLink(params, function (link) {
                  $scope.link = link;
                  drawGeoJson(link.location);
              });


          });
      };
  }
]);
