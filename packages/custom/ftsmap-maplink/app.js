'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var FtsmapMaplink = new Module('ftsmap-maplink');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
FtsmapMaplink.register(function (app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
   


    //FtsmapMaplink.aggregateAsset('js', 'leaflet.js');
    //FtsmapMaplink.aggregateAsset('js', 'angular-leaflet-directive.js');
    //angular-leaflet-directive
    //FtsmapMaplink.aggregateAsset('js', '../lib/ng-clip/src/ngClip.js', {
    //    absolute: false,
    //    global: true
    //});

    FtsmapMaplink.menus.add({
        'roles': ['anonymous'],
        'title': 'MapLink API',
        'dropdown': [
            {
                'title': 'Documentation',
                'link': 'maplink documentation'
            },
            //{
            //    'divider': true
            //},
            {
                'title': 'Playground',
                'link': 'maplink playground'
            }
        ]
    });

    //FtsmapMaplink.aggregateAsset('css', 'ftsmapMaplink.css');

    /**
      //Uncomment to use. Requires meanio@0.3.7 or above
      // Save settings with callback
      // Use this for saving data from administration pages
      FtsmapMaplink.settings({
          'someSetting': 'some value'
      }, function(err, settings) {
          //you now have the settings object
      });
  
      // Another save settings example this time with no callback
      // This writes over the last settings.
      FtsmapMaplink.settings({
          'anotherSettings': 'some value'
      });
  
      // Get settings. Retrieves latest saved settigns
      FtsmapMaplink.settings(function(err, settings) {
          //you now have the settings object
      });
      */

    FtsmapMaplink.angularDependencies(['leaflet-directive']);
    FtsmapMaplink.routes(app, auth, database);

    return FtsmapMaplink;
});
