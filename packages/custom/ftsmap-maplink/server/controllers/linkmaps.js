﻿'use strict';

var mongoose = require('mongoose'),
    LinkMap = mongoose.model('LinkMap');
//_ = require('lodash');
//var GeoJSON = require('geojson');
//var connection;

exports.getlink = function (req, res) {
    var lat = +(req.query.lat);
    var lng = +(req.query.lng);
    var detail = req.query.detail || 'false';

    if (lat && lng) {
        var point = { type: 'Point', coordinates: [lng, lat] };

        LinkMap.geoNear(point, { maxDistance: 1, spherical: true, limit: 1 }, function (err, data) {
            if (!err) {
                if (data) {
                    var firstLink = data[0].obj;
                    
                    var linkObject = {};
                    if (detail === 'true') {
                        linkObject = data[0].obj;
                    }
                    else {
                        //Link Object
                        linkObject.linkId = firstLink.linkId;
                        linkObject.location = firstLink.location;
                        linkObject.upStream = firstLink.upStream;
                        linkObject.downStream = firstLink.downStream;
                    }
                    res.send(linkObject);
                }
                else {
                    res.send(404);
                }
            } else {
                console.log(err);
                res.send(500, 'Unexpected Error');
            }
        });
    } else {
        res.send(403, 'Invalid Parameters');
    }
};