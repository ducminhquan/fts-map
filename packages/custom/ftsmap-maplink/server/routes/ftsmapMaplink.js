//'use strict';

//// The Package is past automatically as first parameter
//module.exports = function(FtsmapMaplink, app, auth, database) {

//  app.get('/ftsmapMaplink/example/anyone', function(req, res, next) {
//    res.send('Anyone can access this');
//  });

//  app.get('/ftsmapMaplink/example/auth', auth.requiresLogin, function(req, res, next) {
//    res.send('Only authenticated users can access this');
//  });

//  app.get('/ftsmapMaplink/example/admin', auth.requiresAdmin, function(req, res, next) {
//    res.send('Only users with Admin role can access this');
//  });

//  app.get('/ftsmapMaplink/example/render', function(req, res, next) {
//    FtsmapMaplink.render('index', {
//      package: 'ftsmap-maplink'
//    }, function(err, html) {
//      //Rendering a view from the Package server/views
//      res.send(html);
//    });
//  });
//};

'use strict';

var linkmaps = require('../controllers/linkmaps');

module.exports = function (FtsmapMaplink, app, auth, database) {
    app.route('/maplink/get')
        .get(linkmaps.getlink);
};
