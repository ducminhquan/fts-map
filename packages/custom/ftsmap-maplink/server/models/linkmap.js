﻿'use strict';

/**
* Module dependencies.
*/
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* Layout Schema
*/
var LinkMapSchema = new Schema({
    linkId: {
        type: String,
        required: true,
        unique: true
    },
    location: {
        type: {},
        required: true        
    },
    oneway: {
        type: String
    },
    upStream: [],
    downStream: [],
    refLink: [],
    osmRef: [],
    descriptions: {
        type: {}
    },
    rawData: {
        type: {}
    },
    systems: [{
        type: String
    }],
    created: {
        type: Date,
        'default': Date.now
    }
});

LinkMapSchema.index({ 'location.features.geometry': '2dsphere'});

/**
* Validations
*/
//LayoutSchema.path('name').validate(function (name) {
//    return !!name;
//}, 'Name cannot be blank');

/**
* Statics
*/
LinkMapSchema.statics.load = function (id, cb) {
    this.findOne({
        _id: id
    })
    //.populate('devices')
    //.populate('user', 'name username')
    .exec(cb);
};

mongoose.model('LinkMap', LinkMapSchema);