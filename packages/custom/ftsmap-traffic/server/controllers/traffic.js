﻿'use strict';

//var mongoose = require('mongoose'),
//    LinkMap = mongoose.model('LinkMap');
//_ = require('lodash');
//var GeoJSON = require('geojson');
//var connection;

var Client = require('node-rest-client').Client;
var restOptions = {};

var proxy_host = process.env.PROXY_HOST;
var proxy_port = process.env.PROXY_PORT;

if (proxy_host && proxy_port) {
    console.info('node-rest-client behide proxy ' + proxy_host + ':' + proxy_port);
    restOptions = {
        proxy: {
            host: proxy_host,
            port: proxy_port,
            tunnel: false
        }
    };
}

var restClient = new Client(restOptions);

var getMessagesUrl = 'http://notis.stis.vn/0/api/v2/messages/all';


exports.getMessages = function (req, res) {
    var minutes_ago = +(req.query.minutes_ago);

    var args = {
        headers: { 'Content-Type': 'application/json' }
    };

    if (minutes_ago) {
        args.parameters = { minutes_ago: minutes_ago };
    }

    restClient.get(getMessagesUrl, args, function (data, response) {
        console.info(data.result);
        data = JSON.parse(data);
        if (data.result) {
            res.jsonp(data.result);
        } else {
            res.send(404);
        }
    });
};