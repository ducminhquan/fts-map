'use strict';

angular.module('mean.ftsmap-traffic').controller('TrafficLiveController', ['$scope', '$stateParams', 'Global', 'FtsmapTraffic',
  function ($scope, $stateParams, Global, FtsmapTraffic) {
      $scope.global = Global;
      $scope.package = {
          name: 'ftsmap-traffic'
      };

      $scope.center = {
          lat: 10.7863767,
          lng: 106.6758813,
          zoom: 13
      };

      $scope.markers = [];

      $scope.layers = {
          baselayers: {
              ftsmap: {
                  name: 'FTS Map',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              }
          },
          overlays: {
              messages: {
                  name: 'Messages',
                  type: 'markercluster',
                  visible: true,
                  layerOptions: {
                      //iconCreateFunction: function (cluster) {
                      //    console.log(cluster);
                      //    var clusterSize = "small";
                      //    if (cluster.getChildCount() >= 10) {
                      //        clusterSize = "medium";
                      //    }
                      //    return new L.DivIcon({
                      //        html: '<div><span>' + cluster.getChildCount() + '</span></div>',
                      //        className: 'marker-cluster marker-cluster-' + clusterSize,
                      //        iconSize: new L.Point(80, 80)
                      //    });
                      //}
                  }
              }
          }
      };

      var typeDisplay = {
          'light_traffic': ['green', 'ok'],
          'heavy_traffic': ['orange', 'remove'],
          'incident': ['orange', 'exclamation-sign'],
          'flood': ['orange', 'warning-sign'],
          'police': ['orange', 'eye-open'],
          'congestion': ['red', 'remove'],
          'road_work': ['orange', 'warning-sign'],
          'fire': ['red', 'fire']
      };

      $scope.liveTrafficInit = function () {
          var params = {};
          var minutes = $stateParams.minutes;
          if (minutes) {
              params.minutes_ago = minutes;
          }
          FtsmapTraffic.query(params, function (messages) {
              //console.info(messages);
              messages.forEach(function (message) {
                  var lat = message.event.location.coordinates[1],
                      lng = message.event.location.coordinates[0],
                      //description = message.event.description.join(', '),
                      description = '<h4>' + message.event.description[0] + '</h4>' + '<h5>' + message.event.description[1] + '</h5>' + message.event.description[2] + '<p>' + (new Date(message.event.created_at)).toGMTString() + '</p>';

                  $scope.markers.push({
                      lat: lat,
                      lng: lng,
                      layer: 'messages',
                      message: description,
                      icon: {
                          type: 'awesomeMarker',
                          markerColor: typeDisplay[message.event.type][0],
                          icon: typeDisplay[message.event.type][1]
                      }
                  });
              });

          });
      };
  }
]);
