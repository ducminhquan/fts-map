'use strict';

angular.module('mean.ftsmap-traffic').config(['$stateProvider',
  function ($stateProvider) {
      $stateProvider
          .state('traffic live', {
              url: '/traffic/:minutes',
              templateUrl: 'ftsmap-traffic/views/live.html'
          })
          //.state('traffic live by id', {
          //    url: '/traffic',
          //    templateUrl: 'ftsmap-traffic/views/live.html'
          //})
      ;
  }
]);
