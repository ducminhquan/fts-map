'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var FtsmapTraffic = new Module('ftsmap-traffic');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
FtsmapTraffic.register(function (app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    FtsmapTraffic.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users
    FtsmapTraffic.menus.add({
        'roles': ['anonymous'],
        'title': 'Live Traffic',
        'link': 'traffic live'
    });

    FtsmapTraffic.aggregateAsset('css', 'ftsmapTraffic.css');

    /**
      //Uncomment to use. Requires meanio@0.3.7 or above
      // Save settings with callback
      // Use this for saving data from administration pages
      FtsmapTraffic.settings({
          'someSetting': 'some value'
      }, function(err, settings) {
          //you now have the settings object
      });
  
      // Another save settings example this time with no callback
      // This writes over the last settings.
      FtsmapTraffic.settings({
          'anotherSettings': 'some value'
      });
  
      // Get settings. Retrieves latest saved settigns
      FtsmapTraffic.settings(function(err, settings) {
          //you now have the settings object
      });
      */

    return FtsmapTraffic;
});
