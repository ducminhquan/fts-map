'use strict';

angular.module('mean.ftsmap').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('map home', {
      url: '/',
      templateUrl: 'ftsmap/views/index.html'
    });
  }
]);
