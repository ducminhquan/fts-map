'use strict';

angular.module('mean.ftsmap').controller('MapController', ['$scope', 'Global', 
  function ($scope, Global) {
      $scope.global = Global;



      $scope.center = {
          lat: 10.7863767,
          lng: 106.6758813,
          zoom: 13
      };

      $scope.markers = [];

      $scope.layers = {
          baselayers: {
              ftsmap: {
                  name: 'FTS Map',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              }
          }
      };
  }
]);
