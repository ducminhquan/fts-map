'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Ftsmap = new Module('ftsmap');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Ftsmap.register(function (app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    Ftsmap.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users
    Ftsmap.menus.add({
        'roles': ['anonymous'],
        'title': 'Map',
        'link': 'map home'
    });

    Ftsmap.aggregateAsset('css', 'ftsmap.css');

    /**
      //Uncomment to use. Requires meanio@0.3.7 or above
      // Save settings with callback
      // Use this for saving data from administration pages
      Ftsmap.settings({
          'someSetting': 'some value'
      }, function(err, settings) {
          //you now have the settings object
      });
  
      // Another save settings example this time with no callback
      // This writes over the last settings.
      Ftsmap.settings({
          'anotherSettings': 'some value'
      });
  
      // Get settings. Retrieves latest saved settigns
      Ftsmap.settings(function(err, settings) {
          //you now have the settings object
      });
      */

    return Ftsmap;
});
