'use strict';

angular.module('mean.ftsmap-stats').config(['$stateProvider',
  function($stateProvider) {
      $stateProvider
          .state('notis heatmap', {
              url: '/stats/notis-heatmap',
              templateUrl: 'ftsmap-stats/views/notis-heatmap.html'
          })
          .state('camera can tho', {
              url: '/stats/camera-can-tho',
              templateUrl: 'ftsmap-stats/views/camera-can-tho.html'
          })
      //.state('traffic live by id', {
      //    url: '/traffic',
      //    templateUrl: 'ftsmap-traffic/views/live.html'
      //})
      ;
  }
]);
