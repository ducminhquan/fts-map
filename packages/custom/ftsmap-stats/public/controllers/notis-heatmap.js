'use strict';

angular.module('mean.ftsmap-stats').controller('FtsmapNotisHeatmapController', ['$scope', 'Global',
  function ($scope, Global) {
      $scope.global = Global;
      $scope.package = {
          name: 'ftsmap-stats'
      };

      $scope.center = {
          lat: 10.7863767,
          lng: 106.6758813,
          zoom: 13
      };

      $scope.markers = [];

      //var testData = {
      //    max: 8,
      //    data: [{lat: 24.6408, lng:46.7728, count: 3},{lat: 50.75, lng:-1.55, count: 1}]
      //};

      //var heatmap = new L.DivHeatmapLayer();

      var hardcodeData = [
    {
        '_id': '9847',
        'sum': 838,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70073,
                10.775017
            ]
        }
    },
    {
        '_id': '23275',
        'sum': 638,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.797847,
                10.85089
            ]
        }
    },
    {
        '_id': '2815',
        'sum': 284,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.72022,
                10.812842
            ]
        }
    },
    {
        '_id': '10257',
        'sum': 128,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7,
                10.8
            ]
        }
    },
    {
        '_id': '26459',
        'sum': 48,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59147,
                10.77075
            ]
        }
    },
    {
        '_id': '105358',
        'sum': 47,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78241,
                21.05982
            ]
        }
    },
    {
        '_id': '107778',
        'sum': 47,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78133,
                21.04762
            ]
        }
    },
    {
        '_id': '22637',
        'sum': 38,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75092,
                10.87619
            ]
        }
    },
    {
        '_id': '32253',
        'sum': 37,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70159,
                10.78658
            ]
        }
    },
    {
        '_id': '109005',
        'sum': 36,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79226,
                21.00556
            ]
        }
    },
    {
        '_id': '6987',
        'sum': 34,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.606666,
                10.753106
            ]
        }
    },
    {
        '_id': '110800',
        'sum': 33,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78076,
                21.03658
            ]
        }
    },
    {
        '_id': null,
        'sum': 32,
        'location': {
            'type': 'Point',
            'coordinates': [
                -18.5333,
                65.9667
            ]
        }
    },
    {
        '_id': '28528',
        'sum': 31,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70245,
                10.7499
            ]
        }
    },
    {
        '_id': '9409',
        'sum': 31,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66889,
                10.74345
            ]
        }
    },
    {
        '_id': '10450',
        'sum': 30,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.82732,
                10.89688
            ]
        }
    },
    {
        '_id': '26835',
        'sum': 30,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76118,
                10.82735
            ]
        }
    },
    {
        '_id': '11297',
        'sum': 30,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.81591,
                10.88314
            ]
        }
    },
    {
        '_id': '117206',
        'sum': 29,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7563572,
                21.2445929
            ]
        }
    },
    {
        '_id': '107594',
        'sum': 29,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8573913574219,
                21.03636156947599
            ]
        }
    },
    {
        '_id': '34197',
        'sum': 28,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.62134,
                10.77368
            ]
        }
    },
    {
        '_id': '9144',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63533,
                10.75772
            ]
        }
    },
    {
        '_id': '104206',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83297,
                21.0329
            ]
        }
    },
    {
        '_id': '35441',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65751,
                10.80099
            ]
        }
    },
    {
        '_id': '26542',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64152,
                10.80234
            ]
        }
    },
    {
        '_id': '105576',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.74567,
                21.04761
            ]
        }
    },
    {
        '_id': '108830',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.795630812645,
                21.00757415248039
            ]
        }
    },
    {
        '_id': '23725',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70626,
                10.78231
            ]
        }
    },
    {
        '_id': '35004',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71203,
                10.76038
            ]
        }
    },
    {
        '_id': '22572',
        'sum': 27,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75483,
                10.87607
            ]
        }
    },
    {
        '_id': '110806',
        'sum': 26,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79336,
                21.03523
            ]
        }
    },
    {
        '_id': '28383',
        'sum': 25,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63673,
                10.80178
            ]
        }
    },
    {
        '_id': '107790',
        'sum': 25,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78104,
                21.04193
            ]
        }
    },
    {
        '_id': '24037',
        'sum': 24,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65832,
                10.79778
            ]
        }
    },
    {
        '_id': '22569',
        'sum': 24,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76318,
                10.87405
            ]
        }
    },
    {
        '_id': '110267',
        'sum': 24,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6198292,
                21.4038877
            ]
        }
    },
    {
        '_id': '10734',
        'sum': 23,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75127,
                10.79232
            ]
        }
    },
    {
        '_id': '22534',
        'sum': 23,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.617187,
                10.721096
            ]
        }
    },
    {
        '_id': '10502',
        'sum': 23,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69959,
                10.79223
            ]
        }
    },
    {
        '_id': '161913',
        'sum': 22,
        'location': {
            'type': 'Point',
            'coordinates': [
                109.286816,
                12.76676
            ]
        }
    },
    {
        '_id': '119030',
        'sum': 22,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7677371,
                21.2351444
            ]
        }
    },
    {
        '_id': '9180',
        'sum': 22,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.58649,
                10.75687
            ]
        }
    },
    {
        '_id': '104167',
        'sum': 21,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84127,
                21.01884
            ]
        }
    },
    {
        '_id': '20374',
        'sum': 21,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77497,
                10.76939
            ]
        }
    },
    {
        '_id': '25263',
        'sum': 21,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.732652,
                10.741851
            ]
        }
    },
    {
        '_id': '108986',
        'sum': 21,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.77471,
                21.03741
            ]
        }
    },
    {
        '_id': '111843',
        'sum': 21,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8059144020081,
                21.04687567710674
            ]
        }
    },
    {
        '_id': '105574',
        'sum': 21,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7508647441046,
                21.04554393120569
            ]
        }
    },
    {
        '_id': '190244',
        'sum': 20,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79943,
                21.00135
            ]
        }
    },
    {
        '_id': '112347',
        'sum': 20,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83037,
                21.00657
            ]
        }
    },
    {
        '_id': '26443',
        'sum': 20,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59647,
                10.79459
            ]
        }
    },
    {
        '_id': '10894',
        'sum': 20,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5914422273636,
                10.7701763130905
            ]
        }
    },
    {
        '_id': '35457',
        'sum': 20,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64731,
                10.80186
            ]
        }
    },
    {
        '_id': '23253',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.792539,
                10.855343
            ]
        }
    },
    {
        '_id': '108136',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8268463611603,
                21.02079153884317
            ]
        }
    },
    {
        '_id': '8714',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60686,
                10.75809
            ]
        }
    },
    {
        '_id': '23115',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69164,
                10.78432
            ]
        }
    },
    {
        '_id': '22580',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77596,
                10.86966
            ]
        }
    },
    {
        '_id': '105528',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8045732975006,
                21.03398828251838
            ]
        }
    },
    {
        '_id': '1520',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70932,
                10.79476
            ]
        }
    },
    {
        '_id': '695',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.57889,
                10.8134
            ]
        }
    },
    {
        '_id': '34186',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60343,
                10.76507
            ]
        }
    },
    {
        '_id': '113818',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86401,
                21.01122
            ]
        }
    },
    {
        '_id': '22535',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6171481,
                10.7196863
            ]
        }
    },
    {
        '_id': '24221',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.641781,
                10.801558
            ]
        }
    },
    {
        '_id': '28392',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64848,
                10.7955
            ]
        }
    },
    {
        '_id': '35043',
        'sum': 18,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.656,
                10.699998
            ]
        }
    },
    {
        '_id': '6366',
        'sum': 17,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64172,
                10.78575
            ]
        }
    },
    {
        '_id': '23449',
        'sum': 16,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71706,
                10.799146
            ]
        }
    },
    {
        '_id': '112343',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82448,
                21.00924
            ]
        }
    },
    {
        '_id': '113822',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8628523349762,
                21.01501064144791
            ]
        }
    },
    {
        '_id': '28483',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68308,
                10.7789
            ]
        }
    },
    {
        '_id': '13024',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6422278434038,
                10.74975730885636
            ]
        }
    },
    {
        '_id': '29120',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.78819,
                10.86726
            ]
        }
    },
    {
        '_id': '33054',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71388,
                10.83529
            ]
        }
    },
    {
        '_id': '114136',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7885814,
                21.0844427
            ]
        }
    },
    {
        '_id': '13650',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6503386,
                10.7438933
            ]
        }
    },
    {
        '_id': '110156',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82823,
                21.02939
            ]
        }
    },
    {
        '_id': '2094',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6200518,
                10.7252936
            ]
        }
    },
    {
        '_id': '4255',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.673193,
                10.779293
            ]
        }
    },
    {
        '_id': '190167',
        'sum': 15,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.93032,
                21.02444
            ]
        }
    },
    {
        '_id': '28323',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6858,
                10.75544
            ]
        }
    },
    {
        '_id': '2558',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.660765,
                10.795537
            ]
        }
    },
    {
        '_id': '5788',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.642007,
                10.771465
            ]
        }
    },
    {
        '_id': '9155',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70946,
                10.80609
            ]
        }
    },
    {
        '_id': '110881',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.73814,
                21.03515
            ]
        }
    },
    {
        '_id': '104817',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82471,
                21.04243
            ]
        }
    },
    {
        '_id': '28386',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64196,
                10.79899
            ]
        }
    },
    {
        '_id': '4518',
        'sum': 14,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6671166,
                10.7896333
            ]
        }
    },
    {
        '_id': '119050',
        'sum': 13,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6072996,
                21.3617166
            ]
        }
    },
    {
        '_id': '148587',
        'sum': 13,
        'location': {
            'type': 'Point',
            'coordinates': [
                107.672338,
                11.417347
            ]
        }
    },
    {
        '_id': '104610',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8567690849304,
                21.01537118906537
            ]
        }
    },
    {
        '_id': '35193',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.56788,
                10.76287
            ]
        }
    },
    {
        '_id': '39339',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7,
                10.5
            ]
        }
    },
    {
        '_id': '35370',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63531,
                10.74812
            ]
        }
    },
    {
        '_id': '41196',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76561,
                10.87464
            ]
        }
    },
    {
        '_id': '107963',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7868278028218,
                21.07805314947866
            ]
        }
    },
    {
        '_id': '110840',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78504,
                21.03655
            ]
        }
    },
    {
        '_id': '10469',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76296,
                10.87396
            ]
        }
    },
    {
        '_id': '27044',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65249,
                10.79068
            ]
        }
    },
    {
        '_id': '33570',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6496,
                10.80174
            ]
        }
    },
    {
        '_id': '105616',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81578,
                21.01794
            ]
        }
    },
    {
        '_id': '9533',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68012,
                10.7931
            ]
        }
    },
    {
        '_id': '110096',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8045196533203,
                21.03479941016134
            ]
        }
    },
    {
        '_id': '190684',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79577,
                21.00062
            ]
        }
    },
    {
        '_id': '109041',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81512,
                21.0165
            ]
        }
    },
    {
        '_id': '34136',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59338,
                10.7516
            ]
        }
    },
    {
        '_id': '28507',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6349,
                10.80726
            ]
        }
    },
    {
        '_id': '9178',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.57407,
                10.7601
            ]
        }
    },
    {
        '_id': '20862',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77266,
                10.77172
            ]
        }
    },
    {
        '_id': '31343',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.62424,
                10.76023
            ]
        }
    },
    {
        '_id': '117951',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86171,
                21.00301
            ]
        }
    },
    {
        '_id': '32060',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68702,
                10.77111
            ]
        }
    },
    {
        '_id': '9387',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63998,
                10.74927
            ]
        }
    },
    {
        '_id': '107577',
        'sum': 12,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7952,
                21.03478
            ]
        }
    },
    {
        '_id': '103607',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.40238,
                20.42751
            ]
        }
    },
    {
        '_id': '3101',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.606667,
                10.753114
            ]
        }
    },
    {
        '_id': '107535',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.77914,
                21.20928
            ]
        }
    },
    {
        '_id': '22563',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.80862,
                10.87304
            ]
        }
    },
    {
        '_id': '9135',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7381682,
                10.702482
            ]
        }
    },
    {
        '_id': '8730',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.82268,
                10.89236
            ]
        }
    },
    {
        '_id': '24288',
        'sum': 11,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63442,
                10.80797
            ]
        }
    },
    {
        '_id': '10763',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75216,
                10.78453
            ]
        }
    },
    {
        '_id': '107775',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78307,
                21.05913
            ]
        }
    },
    {
        '_id': '45835',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.793042,
                10.85039
            ]
        }
    },
    {
        '_id': '4788',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65596,
                10.7925
            ]
        }
    },
    {
        '_id': '23362',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75581,
                10.80891
            ]
        }
    },
    {
        '_id': '23899',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66045,
                10.74663
            ]
        }
    },
    {
        '_id': '41092',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7996024,
                10.8486609
            ]
        }
    },
    {
        '_id': '7115',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.682231,
                10.827476
            ]
        }
    },
    {
        '_id': '46309',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.205716,
                10.768652
            ]
        }
    },
    {
        '_id': '22916',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68386,
                10.75188
            ]
        }
    },
    {
        '_id': '10720',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.677941,
                10.796276
            ]
        }
    },
    {
        '_id': '112651',
        'sum': 10,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.778519,
                21.1393527
            ]
        }
    },
    {
        '_id': '8120',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.609235,
                10.771952
            ]
        }
    },
    {
        '_id': '9623',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6802,
                10.79289
            ]
        }
    },
    {
        '_id': '108708',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.94139,
                21.04475
            ]
        }
    },
    {
        '_id': '114980',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6375233,
                21.4526098
            ]
        }
    },
    {
        '_id': '114993',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6437798,
                21.4555054
            ]
        }
    },
    {
        '_id': '4769',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.73831,
                10.8872
            ]
        }
    },
    {
        '_id': '111881',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8259129524231,
                21.00868088576579
            ]
        }
    },
    {
        '_id': '192848',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.87821,
                21.00127
            ]
        }
    },
    {
        '_id': '32267',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64896,
                10.80178
            ]
        }
    },
    {
        '_id': '40470',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69351,
                10.82196
            ]
        }
    },
    {
        '_id': '36735',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76742,
                10.87242
            ]
        }
    },
    {
        '_id': '10467',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75507,
                10.87591
            ]
        }
    },
    {
        '_id': '29105',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66967,
                10.86164
            ]
        }
    },
    {
        '_id': '107520',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84124,
                21.00517
            ]
        }
    },
    {
        '_id': '104818',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82857,
                21.04106
            ]
        }
    },
    {
        '_id': '111842',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8062040805817,
                21.04813732014002
            ]
        }
    },
    {
        '_id': '42518',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.887527,
                10.972056
            ]
        }
    },
    {
        '_id': '28390',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64672,
                10.79646
            ]
        }
    },
    {
        '_id': '9184',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.58883,
                10.75687
            ]
        }
    },
    {
        '_id': '110861',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.76245,
                21.0414
            ]
        }
    },
    {
        '_id': '29475',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7652074,
                10.8342388
            ]
        }
    },
    {
        '_id': '33716',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.85017,
                10.90312
            ]
        }
    },
    {
        '_id': '114022',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8254408836569,
                21.01116474596417
            ]
        }
    },
    {
        '_id': '104350',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8125,
                21.00918
            ]
        }
    },
    {
        '_id': '26108',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60802,
                10.85788
            ]
        }
    },
    {
        '_id': '28926',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7382615,
                10.7019991
            ]
        }
    },
    {
        '_id': '114450',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81726,
                21.03055
            ]
        }
    },
    {
        '_id': '26464',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69161,
                10.77597
            ]
        }
    },
    {
        '_id': '6359',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63825,
                10.79596
            ]
        }
    },
    {
        '_id': '28174',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75046,
                10.78782
            ]
        }
    },
    {
        '_id': '28537',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70066,
                10.74043
            ]
        }
    },
    {
        '_id': '113816',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8689141273499,
                21.00571622429858
            ]
        }
    },
    {
        '_id': '96447',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.667249,
                20.847313
            ]
        }
    },
    {
        '_id': '31739',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67809,
                10.81355
            ]
        }
    },
    {
        '_id': '12613',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66704,
                10.80023
            ]
        }
    },
    {
        '_id': '190220',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79838,
                21.00059
            ]
        }
    },
    {
        '_id': '26544',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64165,
                10.80233
            ]
        }
    },
    {
        '_id': '27093',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71301,
                10.85968
            ]
        }
    },
    {
        '_id': '29081',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68687,
                10.77505
            ]
        }
    },
    {
        '_id': '189887',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.94327,
                21.01758
            ]
        }
    },
    {
        '_id': '107738',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8676159381867,
                21.04053225431834
            ]
        }
    },
    {
        '_id': '22570',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75921,
                10.87522
            ]
        }
    },
    {
        '_id': '23174',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68522,
                10.7885
            ]
        }
    },
    {
        '_id': '190164',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.9379005432129,
                21.02130006930288
            ]
        }
    },
    {
        '_id': '113817',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86837,
                21.00611
            ]
        }
    },
    {
        '_id': '112318',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81944,
                21.0027
            ]
        }
    },
    {
        '_id': '34211',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6925,
                10.86074
            ]
        }
    },
    {
        '_id': '113249',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.818327665329,
                21.04214442052517
            ]
        }
    },
    {
        '_id': '10466',
        'sum': 9,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75441,
                10.876
            ]
        }
    },
    {
        '_id': '11115',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70013,
                10.72825
            ]
        }
    },
    {
        '_id': '119067',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.610839,
                21.3796983
            ]
        }
    },
    {
        '_id': '114997',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.64416,
                21.4552629
            ]
        }
    },
    {
        '_id': '10577',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71067,
                10.81556
            ]
        }
    },
    {
        '_id': '40477',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.696047,
                10.807866
            ]
        }
    },
    {
        '_id': '10916',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64311,
                10.75404
            ]
        }
    },
    {
        '_id': '27385',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.700834,
                10.739202
            ]
        }
    },
    {
        '_id': '13659',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6736138,
                10.7490702
            ]
        }
    },
    {
        '_id': '107960',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7888967,
                21.0865611
            ]
        }
    },
    {
        '_id': '33416',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60215,
                10.73638
            ]
        }
    },
    {
        '_id': '7897',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68281,
                10.80196
            ]
        }
    },
    {
        '_id': '7766',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.689787,
                10.803515
            ]
        }
    },
    {
        '_id': '30790',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66083,
                10.78893
            ]
        }
    },
    {
        '_id': '13679',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6223998,
                10.7275445
            ]
        }
    },
    {
        '_id': '2299',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.631839,
                10.745685
            ]
        }
    },
    {
        '_id': '119064',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6162964,
                21.3964481
            ]
        }
    },
    {
        '_id': '4931',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.662053,
                10.784492
            ]
        }
    },
    {
        '_id': '158765',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                109.359967,
                12.827508
            ]
        }
    },
    {
        '_id': '13094',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.720147,
                10.798843
            ]
        }
    },
    {
        '_id': '108296',
        'sum': 8,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8390933275223,
                21.00868589359005
            ]
        }
    },
    {
        '_id': '26860',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.8019938,
                10.8477565
            ]
        }
    },
    {
        '_id': '105565',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7803905010223,
                21.0351899515256
            ]
        }
    },
    {
        '_id': '71366',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                107.7,
                10.8
            ]
        }
    },
    {
        '_id': '29477',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.769718,
                10.842341
            ]
        }
    },
    {
        '_id': '42413',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7555816,
                10.8135209
            ]
        }
    },
    {
        '_id': '27504',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5909447,
                10.7674509
            ]
        }
    },
    {
        '_id': '28773',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7374086,
                10.7062804
            ]
        }
    },
    {
        '_id': '31275',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7086628,
                10.7276606
            ]
        }
    },
    {
        '_id': '23869',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6588,
                10.74575
            ]
        }
    },
    {
        '_id': '26839',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77141,
                10.84499
            ]
        }
    },
    {
        '_id': '23346',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.748847,
                10.803243
            ]
        }
    },
    {
        '_id': '28220',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.752495,
                10.7983326
            ]
        }
    },
    {
        '_id': '111851',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79474,
                21.04631
            ]
        }
    },
    {
        '_id': '23401',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76083,
                10.82627
            ]
        }
    },
    {
        '_id': '40486',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69112,
                10.8111
            ]
        }
    },
    {
        '_id': '23269',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.773257,
                10.847625
            ]
        }
    },
    {
        '_id': '26833',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.767886,
                10.839224
            ]
        }
    },
    {
        '_id': '10510',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6915352,
                10.7581624
            ]
        }
    },
    {
        '_id': '26838',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7625993,
                10.8296911
            ]
        }
    },
    {
        '_id': '31855',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6563,
                10.74103
            ]
        }
    },
    {
        '_id': '13663',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6820907,
                10.752181
            ]
        }
    },
    {
        '_id': '107549',
        'sum': 7,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8027482,
                21.0136221
            ]
        }
    },
    {
        '_id': '8716',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.61132,
                10.75801
            ]
        }
    },
    {
        '_id': '105212',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81603,
                21.03054
            ]
        }
    },
    {
        '_id': '33578',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67326,
                10.77105
            ]
        }
    },
    {
        '_id': '28799',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71793,
                10.73818
            ]
        }
    },
    {
        '_id': '107826',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7865273950847,
                21.0119860132222
            ]
        }
    },
    {
        '_id': '111775',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8445596694946,
                21.0078395689035
            ]
        }
    },
    {
        '_id': '148832',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                107.451587,
                11.275823
            ]
        }
    },
    {
        '_id': '73699',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                107.135505,
                10.92132
            ]
        }
    },
    {
        '_id': '107540',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7838924,
                21.1185999
            ]
        }
    },
    {
        '_id': '10995',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.673704,
                10.763477
            ]
        }
    },
    {
        '_id': '40471',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69347,
                10.82211
            ]
        }
    },
    {
        '_id': '11162',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67951,
                10.88177
            ]
        }
    },
    {
        '_id': '190254',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8514904975891,
                21.00803988287245
            ]
        }
    },
    {
        '_id': '9383',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6352105140686,
                10.74773878530105
            ]
        }
    },
    {
        '_id': '9670',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69547,
                10.773326
            ]
        }
    },
    {
        '_id': '7476',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6629981994629,
                10.80312469604658
            ]
        }
    },
    {
        '_id': '10667',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.679476,
                10.767678
            ]
        }
    },
    {
        '_id': '2658',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64657,
                10.76539
            ]
        }
    },
    {
        '_id': '108983',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7710456848145,
                21.03850450504121
            ]
        }
    },
    {
        '_id': '190760',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81697,
                21.00076
            ]
        }
    },
    {
        '_id': '8107',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.710714,
                10.757919
            ]
        }
    },
    {
        '_id': '108127',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80957,
                21.00877
            ]
        }
    },
    {
        '_id': '10956',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70333,
                10.78841
            ]
        }
    },
    {
        '_id': '108573',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8115416765213,
                21.02476014307539
            ]
        }
    },
    {
        '_id': '107799',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7835662363323,
                21.06065304086602
            ]
        }
    },
    {
        '_id': '35464',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64195,
                10.8024
            ]
        }
    },
    {
        '_id': '4172',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64833,
                10.84832
            ]
        }
    },
    {
        '_id': '108950',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78596,
                21.03661
            ]
        }
    },
    {
        '_id': '26847',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.776357,
                10.850875
            ]
        }
    },
    {
        '_id': '107795',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7814003,
                21.0469402
            ]
        }
    },
    {
        '_id': '13703',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70861,
                10.7286753
            ]
        }
    },
    {
        '_id': '110853',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7762491703033,
                21.03718269793237
            ]
        }
    },
    {
        '_id': '107785',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78687,
                21.10307
            ]
        }
    },
    {
        '_id': '28638',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65611,
                10.77771
            ]
        }
    },
    {
        '_id': '189886',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.9359,
                21.02188
            ]
        }
    },
    {
        '_id': '34242',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67738,
                10.83113
            ]
        }
    },
    {
        '_id': '109147',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84261,
                21.02909
            ]
        }
    },
    {
        '_id': '36702',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.687325835228,
                10.8926269439749
            ]
        }
    },
    {
        '_id': '9626',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67761,
                10.79444
            ]
        }
    },
    {
        '_id': '43487',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.84763,
                10.90498
            ]
        }
    },
    {
        '_id': '28388',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64568,
                10.79702
            ]
        }
    },
    {
        '_id': '8728',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.82461,
                10.89491
            ]
        }
    },
    {
        '_id': '114553',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.9288024902344,
                21.09711284116069
            ]
        }
    },
    {
        '_id': '190685',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7961940765381,
                21.00000708190138
            ]
        }
    },
    {
        '_id': '10881',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64614,
                10.76547
            ]
        }
    },
    {
        '_id': '7933',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63897,
                10.796538
            ]
        }
    },
    {
        '_id': '5615',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6774,
                10.77431
            ]
        }
    },
    {
        '_id': '190252',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8506,
                21.00084
            ]
        }
    },
    {
        '_id': '105624',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8477461338043,
                21.00865584671837
            ]
        }
    },
    {
        '_id': '9179',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.58371,
                10.75751
            ]
        }
    },
    {
        '_id': '4770',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.74519,
                10.88302
            ]
        }
    },
    {
        '_id': '10524',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6457194,
                10.7412909
            ]
        }
    },
    {
        '_id': '112939',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.806450843811,
                21.03631150052127
            ]
        }
    },
    {
        '_id': '41649',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7422,
                10.94655
            ]
        }
    },
    {
        '_id': '34212',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69175,
                10.86079
            ]
        }
    },
    {
        '_id': '3638',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7072,
                10.765
            ]
        }
    },
    {
        '_id': '10514',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6769577,
                10.751835
            ]
        }
    },
    {
        '_id': '35201',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.55335,
                10.76983
            ]
        }
    },
    {
        '_id': '105480',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8192932605948,
                21.01353839629749
            ]
        }
    },
    {
        '_id': '103836',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8777117729187,
                21.04710597782313
            ]
        }
    },
    {
        '_id': '23787',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69483,
                10.77943
            ]
        }
    },
    {
        '_id': '33032',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.72396,
                10.86004
            ]
        }
    },
    {
        '_id': '13078',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71133,
                10.80167
            ]
        }
    },
    {
        '_id': '26543',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6375,
                10.80419
            ]
        }
    },
    {
        '_id': '11256',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71677,
                10.79992
            ]
        }
    },
    {
        '_id': '10479',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7377106,
                10.8008174
            ]
        }
    },
    {
        '_id': '108806',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.816712975502,
                21.04312072387445
            ]
        }
    },
    {
        '_id': '114456',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82706,
                21.03203
            ]
        }
    },
    {
        '_id': '26441',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5977274,
                10.8001604
            ]
        }
    },
    {
        '_id': '10177',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68995,
                10.78582
            ]
        }
    },
    {
        '_id': '8726',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.82919,
                10.89785
            ]
        }
    },
    {
        '_id': '9856',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69374,
                10.78243
            ]
        }
    },
    {
        '_id': '104609',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8581745624542,
                21.01290742964154
            ]
        }
    },
    {
        '_id': '104693',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83404,
                21.031
            ]
        }
    },
    {
        '_id': '29118',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.78998,
                10.86714
            ]
        }
    },
    {
        '_id': '110268',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6171684,
                21.3985563
            ]
        }
    },
    {
        '_id': '107804',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78554,
                21.0703649
            ]
        }
    },
    {
        '_id': '35073',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65573,
                10.72015
            ]
        }
    },
    {
        '_id': '112622',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8408957719803,
                21.00421383957988
            ]
        }
    },
    {
        '_id': '19682',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68867,
                10.741
            ]
        }
    },
    {
        '_id': '19883',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7070572,
                10.7699439
            ]
        }
    },
    {
        '_id': '10856',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68137,
                10.86144
            ]
        }
    },
    {
        '_id': '8727',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.82736,
                10.89702
            ]
        }
    },
    {
        '_id': '109143',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84111,
                21.03067
            ]
        }
    },
    {
        '_id': '40490',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69502,
                10.80835
            ]
        }
    },
    {
        '_id': '2925',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71334,
                10.80922
            ]
        }
    },
    {
        '_id': '40392',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66067,
                10.78909
            ]
        }
    },
    {
        '_id': '17009',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75219,
                10.87571
            ]
        }
    },
    {
        '_id': '108952',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7826328277588,
                21.03671205168783
            ]
        }
    },
    {
        '_id': '40550',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68436,
                10.76828
            ]
        }
    },
    {
        '_id': '10628',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68007,
                10.82637
            ]
        }
    },
    {
        '_id': '10173',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6937,
                10.78966
            ]
        }
    },
    {
        '_id': '36324',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.799998,
                10.8
            ]
        }
    },
    {
        '_id': '23697',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70791,
                10.78451
            ]
        }
    },
    {
        '_id': '30785',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66581,
                10.78628
            ]
        }
    },
    {
        '_id': '28197',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.74974,
                10.78708
            ]
        }
    },
    {
        '_id': '190631',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8803939819336,
                21.00872094896648
            ]
        }
    },
    {
        '_id': '107773',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7856371,
                21.0721494
            ]
        }
    },
    {
        '_id': '22554',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.79887,
                10.86452
            ]
        }
    },
    {
        '_id': '109338',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81977,
                21.00294
            ]
        }
    },
    {
        '_id': '9256',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64752,
                10.86094
            ]
        }
    },
    {
        '_id': '110862',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.76142,
                21.04177
            ]
        }
    },
    {
        '_id': '9888',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70147,
                10.79051
            ]
        }
    },
    {
        '_id': '107888',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80593,
                21.03394
            ]
        }
    },
    {
        '_id': '105556',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8145618438721,
                21.00868088576579
            ]
        }
    },
    {
        '_id': '23114',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69057,
                10.78321
            ]
        }
    },
    {
        '_id': '10780',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7406223,
                10.7824024
            ]
        }
    },
    {
        '_id': '104692',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83276,
                21.03139
            ]
        }
    },
    {
        '_id': '10521',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.652178,
                10.74506
            ]
        }
    },
    {
        '_id': '23171',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68269,
                10.79079
            ]
        }
    },
    {
        '_id': '8776',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77775,
                10.85884
            ]
        }
    },
    {
        '_id': '31617',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6235128,
                10.7280079
            ]
        }
    },
    {
        '_id': '190202',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82384,
                21.00205
            ]
        }
    },
    {
        '_id': '18535',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71972,
                10.73202
            ]
        }
    },
    {
        '_id': '28395',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63647,
                10.80215
            ]
        }
    },
    {
        '_id': '10643',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67671,
                10.812
            ]
        }
    },
    {
        '_id': '119034',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6070008,
                21.3544865
            ]
        }
    },
    {
        '_id': '104041',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84944,
                21.01906
            ]
        }
    },
    {
        '_id': '27431',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6630694,
                10.8167924
            ]
        }
    },
    {
        '_id': '13669',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6968595,
                10.7623848
            ]
        }
    },
    {
        '_id': '8721',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.83612,
                10.90033
            ]
        }
    },
    {
        '_id': '8895',
        'sum': 6,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.777238,
                10.851802
            ]
        }
    },
    {
        '_id': '33659',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64943,
                10.75812
            ]
        }
    },
    {
        '_id': '107537',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7799464,
                21.2002744
            ]
        }
    },
    {
        '_id': '4757',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.655273,
                10.811723
            ]
        }
    },
    {
        '_id': '190536',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.997274,
                9.316471
            ]
        }
    },
    {
        '_id': '119051',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6072,
                21.3593065
            ]
        }
    },
    {
        '_id': '28552',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7371294,
                10.7127033
            ]
        }
    },
    {
        '_id': '28548',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7375992,
                10.7053491
            ]
        }
    },
    {
        '_id': '13541',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7,
                10.9
            ]
        }
    },
    {
        '_id': '23252',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.797823,
                10.854374
            ]
        }
    },
    {
        '_id': '41089',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.794623,
                10.853229
            ]
        }
    },
    {
        '_id': '10509',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6975176,
                10.7633342
            ]
        }
    },
    {
        '_id': '113815',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86969,
                21.0048
            ]
        }
    },
    {
        '_id': '118479',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.606926,
                21.3741486
            ]
        }
    },
    {
        '_id': '108981',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7661533355713,
                21.0399664899913
            ]
        }
    },
    {
        '_id': '31609',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6570494,
                10.7475532
            ]
        }
    },
    {
        '_id': '13665',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6839899,
                10.751637
            ]
        }
    },
    {
        '_id': '107777',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78247,
                21.0568
            ]
        }
    },
    {
        '_id': '13672',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6990461,
                10.7645388
            ]
        }
    },
    {
        '_id': '27503',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.590698,
                10.766776
            ]
        }
    },
    {
        '_id': '83117',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.060913,
                11.329265
            ]
        }
    },
    {
        '_id': '3534',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.679992,
                10.80363
            ]
        }
    },
    {
        '_id': '31581',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.667052,
                10.750288
            ]
        }
    },
    {
        '_id': '109269',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.787398,
                21.1079349
            ]
        }
    },
    {
        '_id': '29474',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7621824,
                10.8287962
            ]
        }
    },
    {
        '_id': '107511',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8336699008942,
                21.01462004721209
            ]
        }
    },
    {
        '_id': '114454',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82229,
                21.03125
            ]
        }
    },
    {
        '_id': '27236',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7269114,
                10.7206312
            ]
        }
    },
    {
        '_id': '26439',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5998309,
                10.8099592
            ]
        }
    },
    {
        '_id': '13647',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6438717,
                10.7400523
            ]
        }
    },
    {
        '_id': '13651',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6520563,
                10.74491
            ]
        }
    },
    {
        '_id': '22564',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.81269,
                10.87862
            ]
        }
    },
    {
        '_id': '103713',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.803425,
                21.213851
            ]
        }
    },
    {
        '_id': '22589',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7488,
                10.8748
            ]
        }
    },
    {
        '_id': '27440',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5958523,
                10.7910397
            ]
        }
    },
    {
        '_id': '35458',
        'sum': 5,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64751,
                10.80184
            ]
        }
    },
    {
        '_id': '153460',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                109.295664,
                13.087165
            ]
        }
    },
    {
        '_id': '26814',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.689907,
                10.777507
            ]
        }
    },
    {
        '_id': '23891',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65582,
                10.74403
            ]
        }
    },
    {
        '_id': '23232',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7954932,
                10.8559242
            ]
        }
    },
    {
        '_id': '5086',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.79367,
                10.85116
            ]
        }
    },
    {
        '_id': '109326',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7860376,
                21.0677118
            ]
        }
    },
    {
        '_id': '109268',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.786166,
                21.1103403
            ]
        }
    },
    {
        '_id': '108789',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7874561,
                21.1855311
            ]
        }
    },
    {
        '_id': '7219',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59431,
                10.81563
            ]
        }
    },
    {
        '_id': '13637',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6287692,
                10.7310408
            ]
        }
    },
    {
        '_id': '111527',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.790741,
                21.047405
            ]
        }
    },
    {
        '_id': '33626',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66703,
                10.76767
            ]
        }
    },
    {
        '_id': '26455',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59431,
                10.78433
            ]
        }
    },
    {
        '_id': '3770',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6423284,
                10.7714784
            ]
        }
    },
    {
        '_id': '4865',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.738327,
                10.70071
            ]
        }
    },
    {
        '_id': '4866',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7382251,
                10.7014993
            ]
        }
    },
    {
        '_id': '25486',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.699242,
                10.738484
            ]
        }
    },
    {
        '_id': '8687',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.57018,
                10.83253
            ]
        }
    },
    {
        '_id': '4852',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.750249,
                10.679066
            ]
        }
    },
    {
        '_id': '13652',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.662668,
                10.7497656
            ]
        }
    },
    {
        '_id': '10523',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6478121,
                10.7425493
            ]
        }
    },
    {
        '_id': '23349',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.759642,
                10.824054
            ]
        }
    },
    {
        '_id': '27438',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5954987,
                10.7894456
            ]
        }
    },
    {
        '_id': '10895',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5928392,
                10.7768298
            ]
        }
    },
    {
        '_id': '108894',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78085,
                21.01679
            ]
        }
    },
    {
        '_id': '1636',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.764957,
                10.834724
            ]
        }
    },
    {
        '_id': '31272',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7087758,
                10.7245895
            ]
        }
    },
    {
        '_id': '33546',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.760775,
                10.8263778
            ]
        }
    },
    {
        '_id': '9031',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70395,
                10.82002
            ]
        }
    },
    {
        '_id': '8893',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75686,
                10.81299
            ]
        }
    },
    {
        '_id': '10515',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6698044,
                10.7502582
            ]
        }
    },
    {
        '_id': '13646',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6406008,
                10.7380527
            ]
        }
    },
    {
        '_id': '27441',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59749,
                10.79876
            ]
        }
    },
    {
        '_id': '23351',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.757114,
                10.8157988
            ]
        }
    },
    {
        '_id': '107789',
        'sum': 4,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7810521,
                21.0411406
            ]
        }
    },
    {
        '_id': '29722',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.666576,
                10.976989
            ]
        }
    },
    {
        '_id': '113910',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8415770530701,
                21.02961212264285
            ]
        }
    },
    {
        '_id': '6364',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64116,
                10.78716
            ]
        }
    },
    {
        '_id': '104695',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83645,
                21.03028
            ]
        }
    },
    {
        '_id': '13644',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63656,
                10.73555
            ]
        }
    },
    {
        '_id': '23173',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68513,
                10.78858
            ]
        }
    },
    {
        '_id': '11157',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68044,
                10.86138
            ]
        }
    },
    {
        '_id': '22139',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.72274,
                10.73373
            ]
        }
    },
    {
        '_id': '107510',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83295,
                21.01541
            ]
        }
    },
    {
        '_id': '30795',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67196,
                10.78298
            ]
        }
    },
    {
        '_id': '17913',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.61586,
                10.84335
            ]
        }
    },
    {
        '_id': '27075',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65537,
                10.78038
            ]
        }
    },
    {
        '_id': '18964',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60426,
                10.7263
            ]
        }
    },
    {
        '_id': '103683',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.860288143158,
                21.03763832213561
            ]
        }
    },
    {
        '_id': '189944',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8288687469212,
                21.0002424631794
            ]
        }
    },
    {
        '_id': '109014',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7905507087708,
                21.00363291343356
            ]
        }
    },
    {
        '_id': '29086',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68378,
                10.77668
            ]
        }
    },
    {
        '_id': '11265',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.78684,
                10.75988
            ]
        }
    },
    {
        '_id': '9282',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75496,
                10.78887
            ]
        }
    },
    {
        '_id': '8347',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67861,
                10.77945
            ]
        }
    },
    {
        '_id': '121805',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.781807,
                20.981097
            ]
        }
    },
    {
        '_id': '10561',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6734722256661,
                10.80812262780803
            ]
        }
    },
    {
        '_id': '104163',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8497309684753,
                21.02647262443788
            ]
        }
    },
    {
        '_id': '109730',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.85494,
                21.03475
            ]
        }
    },
    {
        '_id': '34248',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67989,
                10.82673
            ]
        }
    },
    {
        '_id': '190680',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7925,
                21.00455
            ]
        }
    },
    {
        '_id': '109762',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8337557391951,
                21.00834785793044
            ]
        }
    },
    {
        '_id': '113953',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8414053916931,
                21.0193071105253
            ]
        }
    },
    {
        '_id': '108558',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84794,
                21.19193
            ]
        }
    },
    {
        '_id': '104646',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81191,
                21.01215
            ]
        }
    },
    {
        '_id': '32062',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68931,
                10.7737
            ]
        }
    },
    {
        '_id': '28398',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63777,
                10.80404
            ]
        }
    },
    {
        '_id': '1408',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.644156,
                10.793208
            ]
        }
    },
    {
        '_id': '35196',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.56768,
                10.76297
            ]
        }
    },
    {
        '_id': '112322',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8036291599274,
                21.01419940612185
            ]
        }
    },
    {
        '_id': '21253',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75355,
                10.80349
            ]
        }
    },
    {
        '_id': '110841',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78635,
                21.0365
            ]
        }
    },
    {
        '_id': '28664',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68952,
                10.77358
            ]
        }
    },
    {
        '_id': '27089',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7208480834961,
                10.86369496041104
            ]
        }
    },
    {
        '_id': '29186',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59384,
                10.74937
            ]
        }
    },
    {
        '_id': '104812',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81456,
                21.04462
            ]
        }
    },
    {
        '_id': '24510',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60726,
                10.85511
            ]
        }
    },
    {
        '_id': '11168',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71895,
                10.84959
            ]
        }
    },
    {
        '_id': '105197',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8506751060486,
                21.00318219331385
            ]
        }
    },
    {
        '_id': '32259',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69958,
                10.78856
            ]
        }
    },
    {
        '_id': '10656',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6788,
                10.81961
            ]
        }
    },
    {
        '_id': '5938',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.55179,
                10.7706
            ]
        }
    },
    {
        '_id': '24198',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63761,
                10.80428
            ]
        }
    },
    {
        '_id': '23909',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69472,
                10.77979
            ]
        }
    },
    {
        '_id': '34209',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70522,
                10.85935
            ]
        }
    },
    {
        '_id': '24332',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.62961,
                10.823298
            ]
        }
    },
    {
        '_id': '41394',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76559,
                10.87471
            ]
        }
    },
    {
        '_id': '10102',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67422,
                10.76753
            ]
        }
    },
    {
        '_id': '9857',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69153,
                10.78441
            ]
        }
    },
    {
        '_id': '20849',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76081,
                10.77461
            ]
        }
    },
    {
        '_id': '10929',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7125419527292,
                10.80987728522041
            ]
        }
    },
    {
        '_id': '107685',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.87804,
                21.10236
            ]
        }
    },
    {
        '_id': '11732',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.672560274601,
                10.79976547006845
            ]
        }
    },
    {
        '_id': '111214',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7677626609802,
                21.03970613754357
            ]
        }
    },
    {
        '_id': '110370',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80608,
                21.04527
            ]
        }
    },
    {
        '_id': '111550',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78491,
                21.05601
            ]
        }
    },
    {
        '_id': '104603',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8532929420471,
                21.02536602318393
            ]
        }
    },
    {
        '_id': '108132',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82148,
                21.02303
            ]
        }
    },
    {
        '_id': '22918',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.681579,
                10.752659
            ]
        }
    },
    {
        '_id': '147021',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                107.389243,
                11.237297
            ]
        }
    },
    {
        '_id': '107791',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7814783,
                21.0455343
            ]
        }
    },
    {
        '_id': '109193',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7864423,
                21.0920969
            ]
        }
    },
    {
        '_id': '108362',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8459436893463,
                21.03568063025011
            ]
        }
    },
    {
        '_id': '109322',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.92795,
                21.02531
            ]
        }
    },
    {
        '_id': '103742',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7682799,
                21.224927
            ]
        }
    },
    {
        '_id': '104689',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82996,
                21.03216
            ]
        }
    },
    {
        '_id': '3179',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63268,
                10.81368
            ]
        }
    },
    {
        '_id': '107525',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8414697647095,
                21.00146944444439
            ]
        }
    },
    {
        '_id': '23177',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68901,
                10.78498
            ]
        }
    },
    {
        '_id': '107897',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8270663022995,
                21.02930168079067
            ]
        }
    },
    {
        '_id': '9877',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68237,
                10.79127
            ]
        }
    },
    {
        '_id': '8995',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67988,
                10.86155
            ]
        }
    },
    {
        '_id': '112750',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7887697221486,
                21.00236087774826
            ]
        }
    },
    {
        '_id': '11166',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71719,
                10.84123
            ]
        }
    },
    {
        '_id': '22557',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.80353,
                10.86738
            ]
        }
    },
    {
        '_id': '115302',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8188319206238,
                21.04202926637484
            ]
        }
    },
    {
        '_id': '104201',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83592,
                21.03385
            ]
        }
    },
    {
        '_id': '104218',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7775366306305,
                21.00345262554903
            ]
        }
    },
    {
        '_id': '105128',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8017257,
                21.0113697
            ]
        }
    },
    {
        '_id': '104567',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8443611860275,
                21.04147352118021
            ]
        }
    },
    {
        '_id': '7019',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64998,
                10.78216
            ]
        }
    },
    {
        '_id': '9541',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70789,
                10.78453
            ]
        }
    },
    {
        '_id': '24628',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6689,
                10.74158
            ]
        }
    },
    {
        '_id': '33044',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71421,
                10.82787
            ]
        }
    },
    {
        '_id': '27114',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6722464561462,
                10.86181417898275
            ]
        }
    },
    {
        '_id': '24209',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64262,
                10.80455
            ]
        }
    },
    {
        '_id': '9142',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60715,
                10.72842
            ]
        }
    },
    {
        '_id': '112853',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82013,
                21.03533
            ]
        }
    },
    {
        '_id': '104348',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80626,
                21.0135
            ]
        }
    },
    {
        '_id': '34217',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66671,
                10.86163
            ]
        }
    },
    {
        '_id': '1790',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.755299,
                10.861684
            ]
        }
    },
    {
        '_id': '29125',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76936,
                10.87194
            ]
        }
    },
    {
        '_id': '8022',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67202,
                10.78283
            ]
        }
    },
    {
        '_id': '10968',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.717766225338,
                10.79979642781273
            ]
        }
    },
    {
        '_id': '117955',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7950568199158,
                21.01379879445679
            ]
        }
    },
    {
        '_id': '104650',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81726,
                21.03082
            ]
        }
    },
    {
        '_id': '8522',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68297,
                10.75432
            ]
        }
    },
    {
        '_id': '31744',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68655,
                10.82415
            ]
        }
    },
    {
        '_id': '10901',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64243,
                10.76694
            ]
        }
    },
    {
        '_id': '111789',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.817791223526,
                21.08182724262622
            ]
        }
    },
    {
        '_id': '108234',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82008,
                21.00858
            ]
        }
    },
    {
        '_id': '110838',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78279,
                21.0366
            ]
        }
    },
    {
        '_id': '2928',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71637,
                10.8076
            ]
        }
    },
    {
        '_id': '26426',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59745,
                10.69999
            ]
        }
    },
    {
        '_id': '107796',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7816648,
                21.0504644
            ]
        }
    },
    {
        '_id': '26379',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.61506,
                10.84338
            ]
        }
    },
    {
        '_id': '33573',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68128,
                10.77766
            ]
        }
    },
    {
        '_id': '11449',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67181,
                10.77512
            ]
        }
    },
    {
        '_id': '11300',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.80883,
                10.87355
            ]
        }
    },
    {
        '_id': '108338',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8606851100922,
                21.0233731187587
            ]
        }
    },
    {
        '_id': '7993',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67534,
                10.81041
            ]
        }
    },
    {
        '_id': '104838',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.92998,
                21.02697
            ]
        }
    },
    {
        '_id': '8793',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.8479,
                10.90527
            ]
        }
    },
    {
        '_id': '20006',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71438,
                10.82716
            ]
        }
    },
    {
        '_id': '33713',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.85427,
                10.90043
            ]
        }
    },
    {
        '_id': '109144',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84114,
                21.03063
            ]
        }
    },
    {
        '_id': '109064',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82749,
                21.01448
            ]
        }
    },
    {
        '_id': '34595',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59839,
                10.7876
            ]
        }
    },
    {
        '_id': '41197',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76653,
                10.87725
            ]
        }
    },
    {
        '_id': '23121',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69557,
                10.78838
            ]
        }
    },
    {
        '_id': '110825',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79352,
                21.03526
            ]
        }
    },
    {
        '_id': '22900',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6432,
                10.75448
            ]
        }
    },
    {
        '_id': '10986',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68958,
                10.77792
            ]
        }
    },
    {
        '_id': '34568',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63578,
                10.80386
            ]
        }
    },
    {
        '_id': '110080',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7606601715088,
                21.04193914567335
            ]
        }
    },
    {
        '_id': '193724',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7914304733276,
                21.00623704747021
            ]
        }
    },
    {
        '_id': '114831',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8665055036545,
                21.00913910098984
            ]
        }
    },
    {
        '_id': '28625',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65358,
                10.78675
            ]
        }
    },
    {
        '_id': '104758',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83903,
                21.05114
            ]
        }
    },
    {
        '_id': '105552',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8202352502849,
                21.00299188885458
            ]
        }
    },
    {
        '_id': '9487',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70938,
                10.72913
            ]
        }
    },
    {
        '_id': '10420',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.90166,
                10.96753
            ]
        }
    },
    {
        '_id': '26466',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59801,
                10.70593
            ]
        }
    },
    {
        '_id': '9520',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6715289652348,
                10.79188694978101
            ]
        }
    },
    {
        '_id': '28652',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66003,
                10.76368
            ]
        }
    },
    {
        '_id': '103962',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8586573600769,
                21.01939724491449
            ]
        }
    },
    {
        '_id': '189880',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.91185,
                21.03084
            ]
        }
    },
    {
        '_id': '17914',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.61561,
                10.84313
            ]
        }
    },
    {
        '_id': '13673',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7005513,
                10.7655044
            ]
        }
    },
    {
        '_id': '117928',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8517,
                21.03888
            ]
        }
    },
    {
        '_id': '816',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7112459,
                10.7217089
            ]
        }
    },
    {
        '_id': '22576',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76676,
                10.87279
            ]
        }
    },
    {
        '_id': '113700',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8622407913208,
                21.01333808971175
            ]
        }
    },
    {
        '_id': '31274',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7086773,
                10.726769
            ]
        }
    },
    {
        '_id': '115172',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81118,
                21.02512
            ]
        }
    },
    {
        '_id': '31711',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7085862,
                10.731146
            ]
        }
    },
    {
        '_id': '11055',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7086414,
                10.7311189
            ]
        }
    },
    {
        '_id': '4576',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.74937,
                10.8785
            ]
        }
    },
    {
        '_id': '27309',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65505,
                10.86181
            ]
        }
    },
    {
        '_id': '30782',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66635,
                10.78562
            ]
        }
    },
    {
        '_id': '3773',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6415166,
                10.7718798
            ]
        }
    },
    {
        '_id': '9028',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7072,
                10.81586
            ]
        }
    },
    {
        '_id': '111254',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.77311,
                21.05513
            ]
        }
    },
    {
        '_id': '10473',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.79314,
                10.86577
            ]
        }
    },
    {
        '_id': '19151',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6778073459864,
                10.79213791160068
            ]
        }
    },
    {
        '_id': '104202',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8348178863525,
                21.03399829646681
            ]
        }
    },
    {
        '_id': '104440',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8490550518036,
                21.02539606676537
            ]
        }
    },
    {
        '_id': '10141',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68195,
                10.77785
            ]
        }
    },
    {
        '_id': '33579',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67301,
                10.7709
            ]
        }
    },
    {
        '_id': '9253',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6391,
                10.85634
            ]
        }
    },
    {
        '_id': '22573',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75419,
                10.87616
            ]
        }
    },
    {
        '_id': '108236',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8170375227928,
                21.01226143720481
            ]
        }
    },
    {
        '_id': '18890',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.87097,
                10.95147
            ]
        }
    },
    {
        '_id': '31453',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68298,
                10.80174
            ]
        }
    },
    {
        '_id': '31736',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67406,
                10.80888
            ]
        }
    },
    {
        '_id': '8953',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60736,
                10.8552
            ]
        }
    },
    {
        '_id': '23274',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7890527,
                10.8560766
            ]
        }
    },
    {
        '_id': '107815',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.77883,
                21.02758
            ]
        }
    },
    {
        '_id': '8974',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77462,
                10.7701
            ]
        }
    },
    {
        '_id': '23726',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70702,
                10.78326
            ]
        }
    },
    {
        '_id': '22593',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.74686,
                10.87396
            ]
        }
    },
    {
        '_id': '111846',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8027601242065,
                21.04633496967704
            ]
        }
    },
    {
        '_id': '6455',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64747,
                10.80184
            ]
        }
    },
    {
        '_id': '24151',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63574,
                10.80397
            ]
        }
    },
    {
        '_id': '114019',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82395,
                21.00855
            ]
        }
    },
    {
        '_id': '109048',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8238583803177,
                21.0095372212714
            ]
        }
    },
    {
        '_id': '108790',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7807230949402,
                21.19353495044888
            ]
        }
    },
    {
        '_id': '23265',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7651432,
                10.8336887
            ]
        }
    },
    {
        '_id': '108555',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8488833904266,
                21.21423009443214
            ]
        }
    },
    {
        '_id': '109140',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83896,
                21.03284
            ]
        }
    },
    {
        '_id': '44462',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66635,
                10.80479
            ]
        }
    },
    {
        '_id': '104438',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84742,
                21.02589
            ]
        }
    },
    {
        '_id': '4700',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6743019,
                10.7586643
            ]
        }
    },
    {
        '_id': '108523',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8538293838501,
                21.02097959524055
            ]
        }
    },
    {
        '_id': '11724',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75453,
                10.78976
            ]
        }
    },
    {
        '_id': '111839',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8064293861389,
                21.04838764486765
            ]
        }
    },
    {
        '_id': '114311',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7901215553284,
                21.03714264299073
            ]
        }
    },
    {
        '_id': '23785',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69292,
                10.77739
            ]
        }
    },
    {
        '_id': '108151',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.814191699028,
                21.01536618146553
            ]
        }
    },
    {
        '_id': '107598',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83604,
                21.04271
            ]
        }
    },
    {
        '_id': '108117',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80978,
                21.00927
            ]
        }
    },
    {
        '_id': '10492',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71139,
                10.80282
            ]
        }
    },
    {
        '_id': '112356',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8224582672119,
                21.00535565334561
            ]
        }
    },
    {
        '_id': '114123',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81704,
                21.04322
            ]
        }
    },
    {
        '_id': '109149',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8432,
                21.02852
            ]
        }
    },
    {
        '_id': '22577',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76799,
                10.87226
            ]
        }
    },
    {
        '_id': '26425',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59545,
                10.69238
            ]
        }
    },
    {
        '_id': '112886',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83517,
                21.04269
            ]
        }
    },
    {
        '_id': '40482',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69508,
                10.80858
            ]
        }
    },
    {
        '_id': '26827',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.775523,
                10.850514
            ]
        }
    },
    {
        '_id': '3884',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.73679,
                10.80416
            ]
        }
    },
    {
        '_id': '26834',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7645877,
                10.8334277
            ]
        }
    },
    {
        '_id': '9003',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65574,
                10.85512
            ]
        }
    },
    {
        '_id': '107772',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78763,
                21.07878
            ]
        }
    },
    {
        '_id': '24293',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63081,
                10.82558
            ]
        }
    },
    {
        '_id': '44878',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7530713,
                10.8055663
            ]
        }
    },
    {
        '_id': '114826',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86088,
                21.0091
            ]
        }
    },
    {
        '_id': '104275',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86954,
                21.00306
            ]
        }
    },
    {
        '_id': '28162',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7507447,
                10.789456
            ]
        }
    },
    {
        '_id': '26192',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65607,
                10.72006
            ]
        }
    },
    {
        '_id': '10554',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66651,
                10.80485
            ]
        }
    },
    {
        '_id': '20857',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.77005,
                10.77283
            ]
        }
    },
    {
        '_id': '114291',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8580994606018,
                21.03668201038769
            ]
        }
    },
    {
        '_id': '106211',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.893772,
                21.058939
            ]
        }
    },
    {
        '_id': '107755',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8465,
                21.04427
            ]
        }
    },
    {
        '_id': '5566',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70535,
                10.85919
            ]
        }
    },
    {
        '_id': '5603',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.73746,
                10.70517
            ]
        }
    },
    {
        '_id': '35279',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7239439,
                10.7254651
            ]
        }
    },
    {
        '_id': '10174',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69193,
                10.78788
            ]
        }
    },
    {
        '_id': '10897',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5944158,
                10.7843768
            ]
        }
    },
    {
        '_id': '20003',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71431,
                10.8265
            ]
        }
    },
    {
        '_id': '26393',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63937,
                10.84482
            ]
        }
    },
    {
        '_id': '13226',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5951,
                10.69102
            ]
        }
    },
    {
        '_id': '10049',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68542,
                10.78844
            ]
        }
    },
    {
        '_id': '23370',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.741624,
                10.801674
            ]
        }
    },
    {
        '_id': '27386',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70098,
                10.74041
            ]
        }
    },
    {
        '_id': '8123',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.61001,
                10.76136
            ]
        }
    },
    {
        '_id': '107851',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7795429229736,
                21.0293417378402
            ]
        }
    },
    {
        '_id': '11033',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66484,
                10.75909
            ]
        }
    },
    {
        '_id': '111876',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83105,
                21.00629
            ]
        }
    },
    {
        '_id': '23458',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71026,
                10.80112
            ]
        }
    },
    {
        '_id': '44741',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7407382,
                10.7827044
            ]
        }
    },
    {
        '_id': '40795',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67064,
                10.76992
            ]
        }
    },
    {
        '_id': '189965',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8789,
                21.00079
            ]
        }
    },
    {
        '_id': '4977',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.752498,
                10.805094
            ]
        }
    },
    {
        '_id': '108803',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81596,
                21.04068
            ]
        }
    },
    {
        '_id': '109049',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8242017030716,
                21.00931187029911
            ]
        }
    },
    {
        '_id': '109196',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78643,
                21.1001
            ]
        }
    },
    {
        '_id': '3483',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6382,
                10.79542
            ]
        }
    },
    {
        '_id': '22556',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.80193,
                10.86635
            ]
        }
    },
    {
        '_id': '6770',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6850727796555,
                10.75670345644509
            ]
        }
    },
    {
        '_id': '10522',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6493897,
                10.7434761
            ]
        }
    },
    {
        '_id': '6761',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70142,
                10.80383
            ]
        }
    },
    {
        '_id': '28461',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67488,
                10.7996
            ]
        }
    },
    {
        '_id': '13193',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.56997,
                10.66503
            ]
        }
    },
    {
        '_id': '10957',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7043,
                10.78944
            ]
        }
    },
    {
        '_id': '33034',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.72256,
                10.85641
            ]
        }
    },
    {
        '_id': '104694',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.83495,
                21.03076
            ]
        }
    },
    {
        '_id': '30784',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66612,
                10.78611
            ]
        }
    },
    {
        '_id': '8954',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60692,
                10.85586
            ]
        }
    },
    {
        '_id': '39696',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63024,
                10.8202
            ]
        }
    },
    {
        '_id': '109061',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82911,
                21.01719
            ]
        }
    },
    {
        '_id': '189948',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82828,
                21.00081
            ]
        }
    },
    {
        '_id': '108874',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7884800434113,
                21.02566645872593
            ]
        }
    },
    {
        '_id': '34208',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70551,
                10.85931
            ]
        }
    },
    {
        '_id': '23906',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69081,
                10.78344
            ]
        }
    },
    {
        '_id': '8713',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60585,
                10.7571
            ]
        }
    },
    {
        '_id': '3178',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.62954,
                10.82306
            ]
        }
    },
    {
        '_id': '8715',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.60702,
                10.75813
            ]
        }
    },
    {
        '_id': '33640',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65195,
                10.76885
            ]
        }
    },
    {
        '_id': '27058',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65021,
                10.78223
            ]
        }
    },
    {
        '_id': '27215',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.72131,
                10.73807
            ]
        }
    },
    {
        '_id': '34052',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7587012,
                10.8223662
            ]
        }
    },
    {
        '_id': '22585',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.78842,
                10.86713
            ]
        }
    },
    {
        '_id': '109058',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8303117752075,
                21.01950740909395
            ]
        }
    },
    {
        '_id': '104471',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8204036954339,
                21.02411920921387
            ]
        }
    },
    {
        '_id': '1521',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70962,
                10.79508
            ]
        }
    },
    {
        '_id': '7997',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.57078,
                10.8313
            ]
        }
    },
    {
        '_id': '22630',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75116,
                10.87593
            ]
        }
    },
    {
        '_id': '22586',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.79065,
                10.86688
            ]
        }
    },
    {
        '_id': '109343',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82277,
                21.00237
            ]
        }
    },
    {
        '_id': '13658',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6732868,
                10.7491428
            ]
        }
    },
    {
        '_id': '112861',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81051,
                21.02591
            ]
        }
    },
    {
        '_id': '1514',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66338,
                10.77658
            ]
        }
    },
    {
        '_id': '31590',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6882653,
                10.7550433
            ]
        }
    },
    {
        '_id': '108598',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.93039,
                21.02797
            ]
        }
    },
    {
        '_id': '111621',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.803588,
                21.016303
            ]
        }
    },
    {
        '_id': '110850',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7808,
                21.03693
            ]
        }
    },
    {
        '_id': '108126',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8083498477936,
                21.00774942752972
            ]
        }
    },
    {
        '_id': '27301',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64933,
                10.862
            ]
        }
    },
    {
        '_id': '104645',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81361,
                21.01464
            ]
        }
    },
    {
        '_id': '107720',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.88074,
                21.04797
            ]
        }
    },
    {
        '_id': '111236',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.77435,
                21.04233
            ]
        }
    },
    {
        '_id': '9020',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67888,
                10.85135
            ]
        }
    },
    {
        '_id': '108582',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81659,
                21.00584
            ]
        }
    },
    {
        '_id': '27502',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59057,
                10.76505
            ]
        }
    },
    {
        '_id': '9655',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6823,
                10.76608
            ]
        }
    },
    {
        '_id': '33022',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71245,
                10.81008
            ]
        }
    },
    {
        '_id': '125387',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.802296,
                20.992905
            ]
        }
    },
    {
        '_id': '34725',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64803,
                10.77474
            ]
        }
    },
    {
        '_id': '104445',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.85747,
                21.02288
            ]
        }
    },
    {
        '_id': '104595',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8514958620071,
                21.01797511819997
            ]
        }
    },
    {
        '_id': '105577',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.73078,
                21.05565
            ]
        }
    },
    {
        '_id': '107695',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.9079617261887,
                21.07717718577652
            ]
        }
    },
    {
        '_id': '191000',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7937908169333,
                21.00341756954619
            ]
        }
    },
    {
        '_id': '111949',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80647,
                21.04753
            ]
        }
    },
    {
        '_id': '23399',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7581547,
                10.8198239
            ]
        }
    },
    {
        '_id': '28199',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7519,
                10.79185
            ]
        }
    },
    {
        '_id': '106178',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.89275,
                21.05998
            ]
        }
    },
    {
        '_id': '104253',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82167,
                21.03117
            ]
        }
    },
    {
        '_id': '20850',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76162,
                10.77363
            ]
        }
    },
    {
        '_id': '189937',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.86615,
                21.03931
            ]
        }
    },
    {
        '_id': '26384',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.65584,
                10.86191
            ]
        }
    },
    {
        '_id': '41085',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.78862,
                10.854981
            ]
        }
    },
    {
        '_id': '24188',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63444,
                10.80784
            ]
        }
    },
    {
        '_id': '114827',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8608138561249,
                21.00918667516741
            ]
        }
    },
    {
        '_id': '26540',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63757,
                10.80411
            ]
        }
    },
    {
        '_id': '103901',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.85497,
                21.00283
            ]
        }
    },
    {
        '_id': '8699',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66455,
                10.83492
            ]
        }
    },
    {
        '_id': '35097',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6636,
                10.77631
            ]
        }
    },
    {
        '_id': '107579',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79278,
                21.03551
            ]
        }
    },
    {
        '_id': '26410',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70604,
                10.76742
            ]
        }
    },
    {
        '_id': '10480',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7439898,
                10.8019892
            ]
        }
    },
    {
        '_id': '111691',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.786887,
                21.029688
            ]
        }
    },
    {
        '_id': '109011',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7926106452942,
                21.00557600236485
            ]
        }
    },
    {
        '_id': '7665',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5516389161348,
                10.77047011109583
            ]
        }
    },
    {
        '_id': '108773',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8198565244675,
                21.04207933340768
            ]
        }
    },
    {
        '_id': '39257',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.72187,
                10.98416
            ]
        }
    },
    {
        '_id': '112658',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7850897312164,
                21.11393819396229
            ]
        }
    },
    {
        '_id': '160107',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                109.146525,
                12.506137
            ]
        }
    },
    {
        '_id': '23305',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7724,
                10.84962
            ]
        }
    },
    {
        '_id': '10576',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71115,
                10.81646
            ]
        }
    },
    {
        '_id': '105359',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78301,
                21.05899
            ]
        }
    },
    {
        '_id': '23786',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69378,
                10.77833
            ]
        }
    },
    {
        '_id': '107551',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80033,
                21.01142
            ]
        }
    },
    {
        '_id': '40413',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.701567,
                10.765298
            ]
        }
    },
    {
        '_id': '26539',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66054,
                10.80085
            ]
        }
    },
    {
        '_id': '10146',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68139,
                10.77773
            ]
        }
    },
    {
        '_id': '30794',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67215,
                10.78287
            ]
        }
    },
    {
        '_id': '5084',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.791015,
                10.852817
            ]
        }
    },
    {
        '_id': '104212',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84367,
                21.03018
            ]
        }
    },
    {
        '_id': '29398',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68255,
                10.79092
            ]
        }
    },
    {
        '_id': '10525',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6413427,
                10.7386135
            ]
        }
    },
    {
        '_id': '22584',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.78774,
                10.86716
            ]
        }
    },
    {
        '_id': '23169',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.68159,
                10.79182
            ]
        }
    },
    {
        '_id': '111796',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.80898,
                21.0598
            ]
        }
    },
    {
        '_id': '26812',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.69066,
                10.7769
            ]
        }
    },
    {
        '_id': '34723',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.64773,
                10.77517
            ]
        }
    },
    {
        '_id': '27095',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.73434,
                10.87065
            ]
        }
    },
    {
        '_id': '119115',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6072664,
                21.3609132
            ]
        }
    },
    {
        '_id': '104582',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.830569267273,
                21.01981787134363
            ]
        }
    },
    {
        '_id': '108082',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.81149,
                21.02517
            ]
        }
    },
    {
        '_id': '10927',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63422,
                10.75407
            ]
        }
    },
    {
        '_id': '13668',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6923282,
                10.7586062
            ]
        }
    },
    {
        '_id': '34190',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.61324,
                10.7697
            ]
        }
    },
    {
        '_id': '2929',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.71649,
                10.80756
            ]
        }
    },
    {
        '_id': '110056',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.79055,
                21.04055
            ]
        }
    },
    {
        '_id': '13656',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6684211,
                10.7503126
            ]
        }
    },
    {
        '_id': '26620',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66354,
                10.80265
            ]
        }
    },
    {
        '_id': '13648',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6459012,
                10.7412542
            ]
        }
    },
    {
        '_id': '32258',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70128,
                10.78687
            ]
        }
    },
    {
        '_id': '7087',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.762651,
                10.828088
            ]
        }
    },
    {
        '_id': '191001',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78945,
                21.00898
            ]
        }
    },
    {
        '_id': '30792',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.67006,
                10.78401
            ]
        }
    },
    {
        '_id': '5857',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.604575,
                10.766657
            ]
        }
    },
    {
        '_id': '104042',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8493,
                21.0181
            ]
        }
    },
    {
        '_id': '108026',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8014404773712,
                21.00839043167096
            ]
        }
    },
    {
        '_id': '13638',
        'sum': 3,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6351181,
                10.7346546
            ]
        }
    },
    {
        '_id': '10979',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.696496,
                10.784843
            ]
        }
    },
    {
        '_id': '23857',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.656354,
                10.739239
            ]
        }
    },
    {
        '_id': '109344',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82052,
                21.00311
            ]
        }
    },
    {
        '_id': '891',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.650417,
                10.744093
            ]
        }
    },
    {
        '_id': '2831',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.677911,
                10.805171
            ]
        }
    },
    {
        '_id': '107783',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7808953,
                21.0403406
            ]
        }
    },
    {
        '_id': '107797',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.782508,
                21.0564788
            ]
        }
    },
    {
        '_id': '107798',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7830177,
                21.0581277
            ]
        }
    },
    {
        '_id': '107770',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7879753,
                21.0830162
            ]
        }
    },
    {
        '_id': '108782',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7782095,
                21.154477
            ]
        }
    },
    {
        '_id': '115876',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7801935,
                21.1618629
            ]
        }
    },
    {
        '_id': '107661',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7989136,
                21.1885926
            ]
        }
    },
    {
        '_id': '109393',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7807481,
                21.1983885
            ]
        }
    },
    {
        '_id': '192756',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106,
                10
            ]
        }
    },
    {
        '_id': '111991',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7049015,
                21.2570658
            ]
        }
    },
    {
        '_id': '111376',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6456742,
                21.3201031
            ]
        }
    },
    {
        '_id': '118477',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6073605,
                21.3652098
            ]
        }
    },
    {
        '_id': '118767',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6176888,
                21.3953067
            ]
        }
    },
    {
        '_id': '104251',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82298,
                21.02976
            ]
        }
    },
    {
        '_id': '114984',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6381215,
                21.4523568
            ]
        }
    },
    {
        '_id': '29422',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.75489,
                10.85103
            ]
        }
    },
    {
        '_id': '28035',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.774167,
                10.849323
            ]
        }
    },
    {
        '_id': '13661',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6788757,
                10.7521004
            ]
        }
    },
    {
        '_id': '103737',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7819092,
                21.2149025
            ]
        }
    },
    {
        '_id': '19780',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.673805,
                10.735635
            ]
        }
    },
    {
        '_id': '10519',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6636998,
                10.7502205
            ]
        }
    },
    {
        '_id': '13691',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7366433,
                10.7806312
            ]
        }
    },
    {
        '_id': '28547',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7376477,
                10.7051494
            ]
        }
    },
    {
        '_id': '28549',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7372245,
                10.7096867
            ]
        }
    },
    {
        '_id': '18513',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7366268,
                10.7143007
            ]
        }
    },
    {
        '_id': '18518',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7348014,
                10.7138354
            ]
        }
    },
    {
        '_id': '9815',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.689553,
                10.778881
            ]
        }
    },
    {
        '_id': '25981',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7314926,
                10.7130812
            ]
        }
    },
    {
        '_id': '27217',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7244007,
                10.7245614
            ]
        }
    },
    {
        '_id': '27218',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.724082,
                10.7251392
            ]
        }
    },
    {
        '_id': '524',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.643979,
                10.778369
            ]
        }
    },
    {
        '_id': '104753',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7814568,
                21.046677
            ]
        }
    },
    {
        '_id': '586',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.790728,
                10.849084
            ]
        }
    },
    {
        '_id': '26859',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.803682,
                10.846485
            ]
        }
    },
    {
        '_id': '23251',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.8013164,
                10.849036
            ]
        }
    },
    {
        '_id': '13680',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6261585,
                10.729602
            ]
        }
    },
    {
        '_id': '267',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6831817,
                10.7511371
            ]
        }
    },
    {
        '_id': '10475',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.757627,
                10.8161489
            ]
        }
    },
    {
        '_id': '21149',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7538949,
                10.8065266
            ]
        }
    },
    {
        '_id': '107784',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7864718,
                21.0864352
            ]
        }
    },
    {
        '_id': '23157',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7522456,
                10.7962734
            ]
        }
    },
    {
        '_id': '23350',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7581602,
                10.8200381
            ]
        }
    },
    {
        '_id': '28165',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7459796,
                10.7855254
            ]
        }
    },
    {
        '_id': '121252',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.822717,
                20.973624
            ]
        }
    },
    {
        '_id': '8630',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.609851,
                10.867354
            ]
        }
    },
    {
        '_id': '31583',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.673396,
                10.7489618
            ]
        }
    },
    {
        '_id': '22915',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6869769,
                10.7543322
            ]
        }
    },
    {
        '_id': '31587',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6833781,
                10.7515816
            ]
        }
    },
    {
        '_id': '31661',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.729296,
                10.718764
            ]
        }
    },
    {
        '_id': '22747',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6636683,
                10.7502859
            ]
        }
    },
    {
        '_id': '7068',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6598882,
                10.8170278
            ]
        }
    },
    {
        '_id': '23270',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7739541,
                10.8489388
            ]
        }
    },
    {
        '_id': '107780',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7810645,
                21.0438316
            ]
        }
    },
    {
        '_id': '10478',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7339232,
                10.8001165
            ]
        }
    },
    {
        '_id': '3168',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6749151,
                10.7498048
            ]
        }
    },
    {
        '_id': '13655',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6653053,
                10.7505573
            ]
        }
    },
    {
        '_id': '18515',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7331367,
                10.7135174
            ]
        }
    },
    {
        '_id': '892',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6484503,
                10.7429868
            ]
        }
    },
    {
        '_id': '23266',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.767053,
                10.835973
            ]
        }
    },
    {
        '_id': '10967',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.717998,
                10.799615
            ]
        }
    },
    {
        '_id': '104435',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84448,
                21.02673
            ]
        }
    },
    {
        '_id': '33417',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59744,
                10.74389
            ]
        }
    },
    {
        '_id': '28189',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7316063,
                10.778139
            ]
        }
    },
    {
        '_id': '13666',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.686354,
                10.7530609
            ]
        }
    },
    {
        '_id': '117966',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.787128,
                21.030136
            ]
        }
    },
    {
        '_id': '11415',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7173753,
                10.721854
            ]
        }
    },
    {
        '_id': '104721',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.82492,
                21.02751
            ]
        }
    },
    {
        '_id': '2267',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.758833,
                10.819637
            ]
        }
    },
    {
        '_id': '11014',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.70257,
                10.74523
            ]
        }
    },
    {
        '_id': '2265',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76134,
                10.825622
            ]
        }
    },
    {
        '_id': '27234',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7285319,
                10.7180347
            ]
        }
    },
    {
        '_id': '22410',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6208737,
                10.7266654
            ]
        }
    },
    {
        '_id': '28196',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7171875,
                10.771134
            ]
        }
    },
    {
        '_id': '107521',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.84126,
                21.00355
            ]
        }
    },
    {
        '_id': '28505',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.725384,
                10.755845
            ]
        }
    },
    {
        '_id': '24152',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.63581,
                10.80375
            ]
        }
    },
    {
        '_id': '110079',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7879392,
                21.0844144
            ]
        }
    },
    {
        '_id': '110259',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7142004,
                21.2541038
            ]
        }
    },
    {
        '_id': '13643',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6318177,
                10.7327681
            ]
        }
    },
    {
        '_id': '28187',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7218626,
                10.773072
            ]
        }
    },
    {
        '_id': '31710',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7085339,
                10.7309804
            ]
        }
    },
    {
        '_id': '11192',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.76557,
                10.83565
            ]
        }
    },
    {
        '_id': '28219',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7528792,
                10.800478
            ]
        }
    },
    {
        '_id': '28149',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7525126,
                10.8070222
            ]
        }
    },
    {
        '_id': '31616',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6209002,
                10.7266166
            ]
        }
    },
    {
        '_id': '120362',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.78176,
                20.981145
            ]
        }
    },
    {
        '_id': '28234',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7140757,
                10.7706374
            ]
        }
    },
    {
        '_id': '31580',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.665369,
                10.750355
            ]
        }
    },
    {
        '_id': '27442',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5993143,
                10.8075996
            ]
        }
    },
    {
        '_id': '10746',
        'sum': 2,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7484137,
                10.7866463
            ]
        }
    },
    {
        '_id': '9015',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.680129,
                10.839029
            ]
        }
    },
    {
        '_id': '151477',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                109.280704,
                13.171172
            ]
        }
    },
    {
        '_id': '116660',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.776676,
                21.125909
            ]
        }
    },
    {
        '_id': '20942',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.748987,
                10.78491
            ]
        }
    },
    {
        '_id': '103955',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.856896,
                21.029242
            ]
        }
    },
    {
        '_id': '121879',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.781792,
                20.981094
            ]
        }
    },
    {
        '_id': '886',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.655918,
                10.747286
            ]
        }
    },
    {
        '_id': '894',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6404926,
                10.7381332
            ]
        }
    },
    {
        '_id': '190015',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                104.43218,
                22.099727
            ]
        }
    },
    {
        '_id': '103704',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.89647,
                21.06377
            ]
        }
    },
    {
        '_id': '190253',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.85092,
                21.00306
            ]
        }
    },
    {
        '_id': '120732',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.460392,
                20.749814
            ]
        }
    },
    {
        '_id': '141815',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.648484,
                9.823376
            ]
        }
    },
    {
        '_id': '10022',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.698289,
                10.781576
            ]
        }
    },
    {
        '_id': '63309',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                107.113401,
                10.872529
            ]
        }
    },
    {
        '_id': '107794',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7815219,
                21.0463735
            ]
        }
    },
    {
        '_id': '107802',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7853782,
                21.0681216
            ]
        }
    },
    {
        '_id': '107811',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7882965,
                21.0830303
            ]
        }
    },
    {
        '_id': '114140',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.786243,
                21.0900314
            ]
        }
    },
    {
        '_id': '119790',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8279527,
                20.9530122
            ]
        }
    },
    {
        '_id': '109200',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7841964,
                21.1141432
            ]
        }
    },
    {
        '_id': '112656',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7841859,
                21.1168471
            ]
        }
    },
    {
        '_id': '116715',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7793952,
                21.1359364
            ]
        }
    },
    {
        '_id': '116659',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7781059,
                21.1399477
            ]
        }
    },
    {
        '_id': '112653',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7799012,
                21.169864
            ]
        }
    },
    {
        '_id': '115870',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7783438,
                21.164965
            ]
        }
    },
    {
        '_id': '111903',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7829636,
                21.2168821
            ]
        }
    },
    {
        '_id': '103738',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7801151,
                21.2154713
            ]
        }
    },
    {
        '_id': '105492',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7725813,
                21.2219181
            ]
        }
    },
    {
        '_id': '112281',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7663029,
                21.2430371
            ]
        }
    },
    {
        '_id': '110256',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7261508,
                21.272589
            ]
        }
    },
    {
        '_id': '112076',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7309252,
                21.2679458
            ]
        }
    },
    {
        '_id': '119040',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6147323,
                21.3975818
            ]
        }
    },
    {
        '_id': '119103',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.5764081,
                21.3945105
            ]
        }
    },
    {
        '_id': '108474',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.5398503,
                21.385061
            ]
        }
    },
    {
        '_id': '119110',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6428852,
                21.3446842
            ]
        }
    },
    {
        '_id': '119043',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.61523,
                21.3909488
            ]
        }
    },
    {
        '_id': '119069',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6134397,
                21.3828544
            ]
        }
    },
    {
        '_id': '119075',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6200208,
                21.3965976
            ]
        }
    },
    {
        '_id': '112855',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8265620470456,
                21.03478438930008
            ]
        }
    },
    {
        '_id': '22824',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.660254,
                10.754747
            ]
        }
    },
    {
        '_id': '27237',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7262537,
                10.7216249
            ]
        }
    },
    {
        '_id': '21261',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7548893,
                10.8074295
            ]
        }
    },
    {
        '_id': '28170',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.717833,
                10.77195
            ]
        }
    },
    {
        '_id': '29476',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.772087,
                10.846392
            ]
        }
    },
    {
        '_id': '119061',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.6044788,
                21.3953719
            ]
        }
    },
    {
        '_id': '10690',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.678303,
                10.805659
            ]
        }
    },
    {
        '_id': '37624',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7,
                10.599998
            ]
        }
    },
    {
        '_id': '18536',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7257359,
                10.722351
            ]
        }
    },
    {
        '_id': '88142',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.110075,
                11.338609
            ]
        }
    },
    {
        '_id': '27437',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5953666,
                10.7888111
            ]
        }
    },
    {
        '_id': '10507',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6986635,
                10.7643975
            ]
        }
    },
    {
        '_id': '6773',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.694987,
                10.753041
            ]
        }
    },
    {
        '_id': '107550',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8015686,
                21.0124092
            ]
        }
    },
    {
        '_id': '1631',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.758296,
                10.822034
            ]
        }
    },
    {
        '_id': '23039',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.66096,
                10.755616
            ]
        }
    },
    {
        '_id': '35292',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7235777,
                10.7256203
            ]
        }
    },
    {
        '_id': '28774',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7376968,
                10.7050487
            ]
        }
    },
    {
        '_id': '266',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6781128,
                10.7516128
            ]
        }
    },
    {
        '_id': '29364',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6676257,
                10.7504547
            ]
        }
    },
    {
        '_id': '28550',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7371497,
                10.7120709
            ]
        }
    },
    {
        '_id': '11414',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7179245,
                10.7222104
            ]
        }
    },
    {
        '_id': '28769',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7369705,
                10.7141232
            ]
        }
    },
    {
        '_id': '18516',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7321299,
                10.7132899
            ]
        }
    },
    {
        '_id': '30990',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7304957,
                10.7144277
            ]
        }
    },
    {
        '_id': '27232',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7292641,
                10.7168646
            ]
        }
    },
    {
        '_id': '31286',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7272796,
                10.7200962
            ]
        }
    },
    {
        '_id': '40801',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7228791,
                10.7251598
            ]
        }
    },
    {
        '_id': '8468',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7221159,
                10.7247168
            ]
        }
    },
    {
        '_id': '8467',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7210994,
                10.7241232
            ]
        }
    },
    {
        '_id': '11413',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7187429,
                10.7227204
            ]
        }
    },
    {
        '_id': '31332',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7097589,
                10.7231993
            ]
        }
    },
    {
        '_id': '19720',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7095834,
                10.7234318
            ]
        }
    },
    {
        '_id': '31273',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7087052,
                10.7258784
            ]
        }
    },
    {
        '_id': '31709',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7085116,
                10.7304671
            ]
        }
    },
    {
        '_id': '158890',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                109.226839,
                12.699384
            ]
        }
    },
    {
        '_id': '9542',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.704085,
                10.788069
            ]
        }
    },
    {
        '_id': '13509',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.787756,
                10.857947
            ]
        }
    },
    {
        '_id': '108664',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7880115,
                21.0816181
            ]
        }
    },
    {
        '_id': '107630',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7800554,
                21.2061611
            ]
        }
    },
    {
        '_id': '109238',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.779843,
                21.183307
            ]
        }
    },
    {
        '_id': '28032',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7531774,
                10.8046818
            ]
        }
    },
    {
        '_id': '31707',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7085458,
                10.7296524
            ]
        }
    },
    {
        '_id': '25292',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7000377,
                10.7388431
            ]
        }
    },
    {
        '_id': '11004',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.700303,
                10.738963
            ]
        }
    },
    {
        '_id': '24219',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.640912,
                10.801134
            ]
        }
    },
    {
        '_id': '82994',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.10177,
                11.299218
            ]
        }
    },
    {
        '_id': '144047',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                108.038594,
                12.6797394
            ]
        }
    },
    {
        '_id': '13662',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6805628,
                10.7523449
            ]
        }
    },
    {
        '_id': '104752',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7814846,
                21.0456914
            ]
        }
    },
    {
        '_id': '105127',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.802094,
                21.011017
            ]
        }
    },
    {
        '_id': '9838',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.702894,
                10.770017
            ]
        }
    },
    {
        '_id': '10518',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6637948,
                10.7502498
            ]
        }
    },
    {
        '_id': '4189',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6201554,
                10.7255168
            ]
        }
    },
    {
        '_id': '193723',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7785987854004,
                21.02470506286007
            ]
        }
    },
    {
        '_id': '13508',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7875454,
                10.857727
            ]
        }
    },
    {
        '_id': '26850',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7860752,
                10.8567856
            ]
        }
    },
    {
        '_id': '26849',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7825106,
                10.8548659
            ]
        }
    },
    {
        '_id': '26445',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.596227,
                10.7929783
            ]
        }
    },
    {
        '_id': '23248',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.8002188,
                10.8543539
            ]
        }
    },
    {
        '_id': '26840',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7724744,
                10.8467187
            ]
        }
    },
    {
        '_id': '22784',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6635483,
                10.7500708
            ]
        }
    },
    {
        '_id': '221',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7708546,
                10.8451247
            ]
        }
    },
    {
        '_id': '18651',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7250829,
                10.7233514
            ]
        }
    },
    {
        '_id': '23352',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7579293,
                10.8181942
            ]
        }
    },
    {
        '_id': '23353',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7576124,
                10.8171389
            ]
        }
    },
    {
        '_id': '10487',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7561268,
                10.80933
            ]
        }
    },
    {
        '_id': '23363',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7554355,
                10.808128
            ]
        }
    },
    {
        '_id': '23155',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7534615,
                10.8026264
            ]
        }
    },
    {
        '_id': '22722',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.660317,
                10.754629
            ]
        }
    },
    {
        '_id': '23156',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7531638,
                10.8012479
            ]
        }
    },
    {
        '_id': '28221',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7511798,
                10.7917971
            ]
        }
    },
    {
        '_id': '10476',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.732604,
                10.7997505
            ]
        }
    },
    {
        '_id': '28163',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.74973,
                10.7879136
            ]
        }
    },
    {
        '_id': '28211',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7382858,
                10.7815462
            ]
        }
    },
    {
        '_id': '28161',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.734054,
                10.7793572
            ]
        }
    },
    {
        '_id': '13701',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7278326,
                10.7761658
            ]
        }
    },
    {
        '_id': '28216',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7157697,
                10.7710363
            ]
        }
    },
    {
        '_id': '23094',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6997611,
                10.764844
            ]
        }
    },
    {
        '_id': '31593',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6944274,
                10.7593538
            ]
        }
    },
    {
        '_id': '27235',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7276584,
                10.7194644
            ]
        }
    },
    {
        '_id': '111687',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.785669,
                21.02587
            ]
        }
    },
    {
        '_id': '27439',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5956379,
                10.7901227
            ]
        }
    },
    {
        '_id': '13654',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.664434,
                10.7503636
            ]
        }
    },
    {
        '_id': '109418',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.781139,
                21.1940807
            ]
        }
    },
    {
        '_id': '28772',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7371854,
                10.7103299
            ]
        }
    },
    {
        '_id': '31606',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6453359,
                10.7407408
            ]
        }
    },
    {
        '_id': '13635',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6359531,
                10.7352881
            ]
        }
    },
    {
        '_id': '13640',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6305522,
                10.7323063
            ]
        }
    },
    {
        '_id': '31350',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7091497,
                10.723811
            ]
        }
    },
    {
        '_id': '31615',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6202436,
                10.7263049
            ]
        }
    },
    {
        '_id': '27231',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7298086,
                10.7159765
            ]
        }
    },
    {
        '_id': '103740',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7750048,
                21.2196056
            ]
        }
    },
    {
        '_id': '23348',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.759704,
                10.8243898
            ]
        }
    },
    {
        '_id': '31586',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6800596,
                10.752156
            ]
        }
    },
    {
        '_id': '22948',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6615,
                10.75127
            ]
        }
    },
    {
        '_id': '124966',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.822697,
                20.973961
            ]
        }
    },
    {
        '_id': '23400',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7566835,
                10.8106682
            ]
        }
    },
    {
        '_id': '11016',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6810467,
                10.7510617
            ]
        }
    },
    {
        '_id': '27505',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.59062,
                10.766722
            ]
        }
    },
    {
        '_id': '26837',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7641547,
                10.8325993
            ]
        }
    },
    {
        '_id': '5085',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.792476,
                10.852481
            ]
        }
    },
    {
        '_id': '116592',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7797511,
                21.1318142
            ]
        }
    },
    {
        '_id': '13634',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6288117,
                10.7312634
            ]
        }
    },
    {
        '_id': '26159',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6736646,
                10.7493666
            ]
        }
    },
    {
        '_id': '9897',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.697365,
                10.79275
            ]
        }
    },
    {
        '_id': '13660',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6774654,
                10.7520173
            ]
        }
    },
    {
        '_id': '26162',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6729287,
                10.7495141
            ]
        }
    },
    {
        '_id': '13639',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6316939,
                10.7328087
            ]
        }
    },
    {
        '_id': '10505',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7009136,
                10.7660426
            ]
        }
    },
    {
        '_id': '9391',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6637393,
                10.7503353
            ]
        }
    },
    {
        '_id': '36949',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.8044063,
                10.8454171
            ]
        }
    },
    {
        '_id': '105224',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.8299067616667,
                21.03193666071484
            ]
        }
    },
    {
        '_id': '31953',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7021492,
                10.767529
            ]
        }
    },
    {
        '_id': '98323',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.79159,
                20.816444
            ]
        }
    },
    {
        '_id': '10506',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7003723,
                10.7653935
            ]
        }
    },
    {
        '_id': '41849',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.764077,
                10.837535
            ]
        }
    },
    {
        '_id': '31613',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6332584,
                10.7338438
            ]
        }
    },
    {
        '_id': '8344',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6797,
                10.77885
            ]
        }
    },
    {
        '_id': '108779',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7784113,
                21.1696832
            ]
        }
    },
    {
        '_id': '28929',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.73826,
                10.7018986
            ]
        }
    },
    {
        '_id': '11418',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7123155,
                10.7214679
            ]
        }
    },
    {
        '_id': '28178',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7237878,
                10.7742256
            ]
        }
    },
    {
        '_id': '13657',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6706651,
                10.7500108
            ]
        }
    },
    {
        '_id': '8894',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7853783,
                10.8565927
            ]
        }
    },
    {
        '_id': '26440',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.5991126,
                10.8066455
            ]
        }
    },
    {
        '_id': '28173',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.715859,
                10.771296
            ]
        }
    },
    {
        '_id': '2493',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.665058,
                10.832084
            ]
        }
    },
    {
        '_id': '31657',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6604395,
                10.7489054
            ]
        }
    },
    {
        '_id': '13641',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6295698,
                10.7316756
            ]
        }
    },
    {
        '_id': '124622',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.489736,
                20.896017
            ]
        }
    },
    {
        '_id': '111011',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7232691,
                21.2768343
            ]
        }
    },
    {
        '_id': '107534',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.7807345,
                21.2203021
            ]
        }
    },
    {
        '_id': '28228',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7257369,
                10.7749365
            ]
        }
    },
    {
        '_id': '28205',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7187024,
                10.7719359
            ]
        }
    },
    {
        '_id': '110271',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                105.615838,
                21.3932365
            ]
        }
    },
    {
        '_id': '7088',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7645486,
                10.832042
            ]
        }
    },
    {
        '_id': '40432',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7237053,
                10.7255544
            ]
        }
    },
    {
        '_id': '44906',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.620322,
                10.7261602
            ]
        }
    },
    {
        '_id': '23405',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.758086,
                10.818251
            ]
        }
    },
    {
        '_id': '23154',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7014907,
                10.767003
            ]
        }
    },
    {
        '_id': '28217',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7131666,
                10.7705788
            ]
        }
    },
    {
        '_id': '40532',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7198137,
                10.7233626
            ]
        }
    },
    {
        '_id': '144078',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                108.042259,
                12.67932
            ]
        }
    },
    {
        '_id': '28235',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7153674,
                10.7708982
            ]
        }
    },
    {
        '_id': '1475',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6772347,
                10.7520614
            ]
        }
    },
    {
        '_id': '13671',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6981686,
                10.7639112
            ]
        }
    },
    {
        '_id': '534',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7609851,
                10.8265779
            ]
        }
    },
    {
        '_id': '13667',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.6891874,
                10.7560705
            ]
        }
    },
    {
        '_id': '31483',
        'sum': 1,
        'location': {
            'type': 'Point',
            'coordinates': [
                106.7302433,
                10.715149
            ]
        }
    }
      ];

      var dataPoints = [];

      //hardcodeData.forEach(function (d) {
      ////var d = hardcodeData[0];
      //    var tmp = [];
      //    tmp.push(d.location.coordinates[1], d.location.coordinates[0], d.sum/1000);
      //    dataPoints.push(tmp);
      //});

      //var dataPoints = [
      //             [44.651144316, -63.586260171, 0.5],
      //             [44.75, -63.5, 0.8]];



      $scope.layers = {
          baselayers: {
              ftsmap_color: {
                  name: 'FTS Map Colorful',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              },
              ftsmap_contrast: {
                  name: 'FTS Map Contrast',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              }
          },
          overlays: {
              heatmap: {
                  name: 'Heat Map',
                  type: 'heatmap',
                  data: dataPoints,
                  visible: true,
                  'layerParams': { size: 2000 },
                  'layerOptions': { opacity : 0.6}
              }
          }
      };

      $scope.init = function () {
          hardcodeData.forEach(function (d) {
              var tmp = [];
              tmp.push(d.location.coordinates[1], d.location.coordinates[0], Math.log(d.sum) / 10);
              dataPoints.push(tmp);
          });
      };
  }
]);
