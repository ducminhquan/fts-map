﻿'use strict';

angular.module('mean.ftsmap-stats').controller('FtsmapCameraCanThoController', ['$scope', 'Global',
  function ($scope, Global) {
      $scope.global = Global;
      $scope.package = {
          name: 'ftsmap-stats'
      };

      //Can Tho center
      //10.0436
      //105.7652
      $scope.center = {
          lat: 10.0436,
          lng: 105.7652,
          zoom: 13
      };

      $scope.markers = [];

      //var testData = {
      //    max: 8,
      //    data: [{lat: 24.6408, lng:46.7728, count: 3},{lat: 50.75, lng:-1.55, count: 1}]
      //};

      //var heatmap = new L.DivHeatmapLayer();

      var data = {
          'type': 'FeatureCollection',
          'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.71596703789919,
                      10.099743864142951
                    ]
                },
                'properties': {
                    'name': 'Lê Hồng Phong - KCN Trà Nóc 1',
                    'cameraCount': 6,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.70530815745609,
                      10.100985596589581
                    ]
                },
                'properties': {
                    'name': 'QL 91, KCN Trà Nóc 1',
                    'cameraCount': 2,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.71423084710393,
                      10.077603046970893
                    ]
                },
                'properties': {
                    'name': 'Sân bay Cần Thơ - Võ Văn Kiệt',
                    'cameraCount': 4,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.74446388295627,
                      10.057283284763528
                    ]
                },
                'properties': {
                    'name': 'Võ Văn Kiệt - Trạm biến áp',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.76515444329715,
                      10.043555110278092
                    ]
                },
                'properties': {
                    'name': 'Vòng xoay Võ Văn Kiệt - Nguyễn Văn Cừ - Mậu Thân',
                    'cameraCount': 8,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.76479121960703,
                      10.021476736840626
                    ]
                },
                'properties': {
                    'name': 'Vòng xoay 3/2 - Nguyễn Văn Linh',
                    'cameraCount': 8,
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.76077245019617,
                      10.02428020994607
                    ]
                },
                'properties': {
                    'name': 'Bến xe Cần Thơ - Metro',
                    'cameraCount': 3,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.76844076167286,
                      10.019696532507538
                    ]
                },
                'properties': {
                    'name': 'Vòng xoay 30/4 - Cầu Hưng Lợi',
                    'cameraCount': 8,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.77979207597019,
                      10.029401922136145
                    ]
                },
                'properties': {
                    'name': 'Ngã ba Quang Trung - 30/4',
                    'cameraCount': 6,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.78633532485686,
                      10.011204272931636
                    ]
                },
                'properties': {
                    'name': 'Đường Võ Nguyên Giáp - Trường Trung cấp Bách Nghệ',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.79829261264526,
                      10.005720744362463
                    ]
                },
                'properties': {
                    'name': 'Vòng xoay khu Tây Nam Bộ',
                    'cameraCount': 3,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.81131476013861,
                      9.991757007141814
                    ]
                },
                'properties': {
                    'name': 'Đường Võ Nguyên Giáp (đoạn giữa khu Tây Nam Bộ và cảng Cái Cui)',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.82492965309821,
                      9.977333948890731
                    ]
                },
                'properties': {
                    'name': 'Ngã ba cảng Cái Cui',
                    'cameraCount': 3,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.7947102964399,
                      10.020511661372907
                    ]
                },
                'properties': {
                    'name': 'Đường dẫn cầu Cần Thơ (ngang vị trí H8/2070)',
                    'cameraCount': 4,
                    'type': '1,3,4'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.74720388118546,
                      9.984832145846681
                    ]
                },
                'properties': {
                    'name': 'Nút giao IC4',
                    'cameraCount': 6,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.78007198203763,
                      10.045312865334395
                    ]
                },
                'properties': {
                    'name': 'òng xoay Trần Phú - CMT8 - Nguyễn Trãi - Hùng Vương',
                    'cameraCount': 8,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.77355697630985,
                      10.051834519566626
                    ]
                },
                'properties': {
                    'name': 'Ngã tư CMT8 - Nguyễn Văn Cừ',
                    'cameraCount': 8,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.72611168826879,
                      10.091737477763331
                    ]
                },
                'properties': {
                    'name': 'Ngã ba Lê Hồng Phong - Sân bay Cần Thơ',
                    'cameraCount': 3,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.68846904896657,
                      10.10096120524301
                    ]
                },
                'properties': {
                    'name': 'Đoạn giữa km11 và km13 - QL91',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.67323410176198,
                      10.10510169491539
                    ]
                },
                'properties': {
                    'name': 'Ngã ba nút giao quốc lộ 91 và quốc lộ 91B',
                    'cameraCount': 6,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.65233432911793,
                      10.110763094484
                    ]
                },
                'properties': {
                    'name': 'Đoạn giữa km16 và km18',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.58209268304708,
                      10.14192330044692
                    ]
                },
                'properties': {
                    'name': 'Đoạn giữa km28 và km29',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.52899006464328,
                      10.276413035666408
                    ]
                },
                'properties': {
                    'name': 'Ngã ba mũi tàu km43 QL 91',
                    'cameraCount': 3,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.48612977358793,
                      10.314623301188888
                    ]
                },
                'properties': {
                    'name': 'Ngã ba Lộ Tẻ - Rạch Giá',
                    'cameraCount': 6,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.47464049285544,
                      10.308234529359197
                    ]
                },
                'properties': {
                    'name': 'Quốc lộ 80, km68+300',
                    'cameraCount': 3,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.47245181029929,
                      10.306587841243669
                    ]
                },
                'properties': {
                    'name': 'Km69+500 QL 80',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.6702293376656,
                      10.070744432034672
                    ]
                },
                'properties': {
                    'name': 'Cầu Xẻo Khế QL 91B',
                    'cameraCount': 2,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.66894401219206,
                      10.078057516515159
                    ]
                },
                'properties': {
                    'name': 'Cầu Xẻo Sao',
                    'cameraCount': 2,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.72904619788764,
                      10.047057517356677
                    ]
                },
                'properties': {
                    'name': 'Km6+600 QL 91B (gần quận đội Bình Thủy)',
                    'cameraCount': 2,
                    'type': '1'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.68627967641072,
                      10.062716047438997
                    ]
                },
                'properties': {
                    'name': 'Giữa km4 và km5 QL 91B',
                    'cameraCount': '',
                    'type': '2'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                      105.73851123962046,
                      10.017608901247428
                    ]
                },
                'properties': {
                    'name': 'Đường Nguyễn Văn Cừ (bệnh viện nhi đồng)',
                    'cameraCount': 3,
                    'type': '1'
                }
            }
          ]
      };

      //hardcodeData.forEach(function (d) {
      ////var d = hardcodeData[0];
      //    var tmp = [];
      //    tmp.push(d.location.coordinates[1], d.location.coordinates[0], d.sum/1000);
      //    dataPoints.push(tmp);
      //});

      //var dataPoints = [
      //             [44.651144316, -63.586260171, 0.5],
      //             [44.75, -63.5, 0.8]];

      //1.       Giám sát lưu lượng, phát hiện sự cố

      //2.       Giám sát tốc độ

      //3.       Nhận dạng biển số

      //4.       Bảng thông tin giao thông linh hoạt (mình hay gọi là bảng quang báo) kèm theo cổng long môn


      $scope.layers = {
          baselayers: {
              ftsmap_color: {
                  name: 'FTS Map Colorful',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              },
              ftsmap_contrast: {
                  name: 'FTS Map Contrast',
                  url: 'http://map.stis.vn/{style}/{z}/{x}/{y}.png',
                  type: 'xyz',
                  layerParams: {
                      style: 'bright'
                  }
              }
          },
          overlays: {
              luu_luong: {
                  name: 'Giám sát lưu lượng, phát hiện sự cố',
                  type: 'group',
                  visible: true,
              },
              toc_do: {
                  name: 'Giám sát tốc độ',
                  type: 'group',
                  visible: true,
              },
              bien_so: {
                  name: 'Nhận dạng biển số',
                  type: 'group',
                  visible: true,
              },
              quang_bao: {
                  name: 'Bảng thông tin giao thông linh hoạt',
                  type: 'group',
                  visible: true,
              }
          }
      };

      var typeDisplay = {
          'luu_luong': ['darkred', ''],
          'toc_do': ['purple', ''],
          'bien_so': ['blue', ''],
          'quang_bao': ['cadetblue', '']
      };

      $scope.init = function () {
          data.features.forEach(function (d) {
              //console.log(d.properties.type);
              var lat = d.geometry.coordinates[1],
                  lng = d.geometry.coordinates[0],
                  description = '<h4>' + d.properties.name + '</h4>' + '<h5> Số lượng camera: ' + d.properties.cameraCount + '</h5>';

              var types = [];

              if (d.properties.type.indexOf('1') !== -1) {
                  types.push('luu_luong');
              };
              if (d.properties.type.indexOf('2') !== -1) {
                  types.push('toc_do');
              };
              if (d.properties.type.indexOf('3') !== -1) {
                  types.push('bien_so');
              };
              if (d.properties.type.indexOf('4') !== -1) {
                  types.push('quang_bao');
              };

              types.forEach(function (type) {
                  $scope.markers.push({
                      lat: lat,
                      lng: lng,
                      layer: type,
                      message: description,
                      icon: {
                          type: 'awesomeMarker',
                          markerColor: typeDisplay[type][0],
                          icon: typeDisplay[type][1]
                      }
                  });
              });
          });
      };
  }
]);
