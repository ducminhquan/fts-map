﻿'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var FtsmapStats = new Module('ftsmap-stats');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
FtsmapStats.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  FtsmapStats.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users

  FtsmapStats.menus.add({
      'roles': ['anonymous'],
      'title': 'Statistic',
      'dropdown': [
          {
              'title': 'Notis Footprint - 2014 October',
              'link': 'notis heatmap'
          },
          {
              'title': 'Camera Cần Thơ',
              'link': 'camera can tho'
          }
      ]
  });
  
  FtsmapStats.aggregateAsset('css', 'ftsmapStats.css');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    FtsmapStats.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    FtsmapStats.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    FtsmapStats.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return FtsmapStats;
});
