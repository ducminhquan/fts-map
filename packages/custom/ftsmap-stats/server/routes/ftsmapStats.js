'use strict';

// The Package is past automatically as first parameter
module.exports = function(FtsmapStats, app, auth, database) {

  app.get('/ftsmapStats/example/anyone', function(req, res, next) {
    res.send('Anyone can access this');
  });

  app.get('/ftsmapStats/example/auth', auth.requiresLogin, function(req, res, next) {
    res.send('Only authenticated users can access this');
  });

  app.get('/ftsmapStats/example/admin', auth.requiresAdmin, function(req, res, next) {
    res.send('Only users with Admin role can access this');
  });

  app.get('/ftsmapStats/example/render', function(req, res, next) {
    FtsmapStats.render('index', {
      package: 'ftsmap-stats'
    }, function(err, html) {
      //Rendering a view from the Package server/views
      res.send(html);
    });
  });
};
