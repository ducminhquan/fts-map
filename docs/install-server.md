#Prerequirement

	apt-get install git 

#Installation

##Prepare User
	adduser maprender

##Installing postgresql / postgis

Install `postgresql` and `postgis`

	apt-get install postgresql-9.1-postgis postgresql-contrib postgresql-server-dev-9.1

Now you need to create a postgis database. The defaults of various programs assume the database is called `gis` and we will use the same convention in this tutorial, although this is not necessary.

	su postgres
	createuser maprender # answer yes for superuser
    createdb -E UTF8 -O maprender gis
    exit

Set up PostGIS on the postresql database

	su postgres
    psql -f /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql -d gis
    
This should respond with many lines ending with:
	
    ...
	CREATE FUNCTION
	COMMIT
	...
	DROP FUNCTION

Give your newly-created user permission to access some of the PostGIS extensions’ data:

	psql -d gis -c "ALTER TABLE geometry_columns OWNER TO maprender; ALTER TABLE spatial_ref_sys OWNER TO maprender;"
    
Return to root user:

	exit
    
## Installing osm2pgsql

    apt-get install libprotobuf-c0-dev protobuf-c-compiler
	apt-get install lua5.2 liblua5.2-dev

Build:

    mkdir ~/src
	cd ~/src
	git clone git://github.com/openstreetmap/osm2pgsql.git
	cd osm2pgsql
    ./autogen
    ./configure
    make
    make install
    
    su postgres
    psql -f /usr/local/share/osm2pgsql/900913.sql -d gis
    exit    
    
## Installing Mapnik

	apt-get install libmapnik-dev libmapnik2-2.0 libmapnik2-dev mapnik-doc mapnik-utils python-mapnik2